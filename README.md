![Alt text](https://www.googleapis.com/download/storage/v1/b/gusl-images/o/logo.png?generation=1588764693002370&alt=media)

# Postgres

This project is how to use postgres database

## Getting started

Its worth noting from the outset that one instance of Postgres can manage many databases, thus allowing for developers
have their own database within a shared instance. Those whom love all things Docker, can always run their own local
instance.

### Docker Instance

To install and run, do the following.

```shell
    docker pull postgres
    docker run -d --name postgres-server -p 5432:5432 -e "POSTGRES_PASSWORD=password" postgres
```

To create a volume for postgres

```shell
docker volume create postgres-data
docker run -d --name postgres-server -p 5432:5432 -v postgres-data:/var/lib/postgresql/data -e "POSTGRES_PASSWORD=password" postgres

```

## Postgres Config

```java
    @JsonDeserialize(using = SystemProperyDeserializer.class)
private String database;
@JsonDeserialize(using = SystemProperyDeserializer.class)
private String connectionPoints;
private Integer port;
private String username;
@JsonDeserialize(using = ObfuscationDeserializer.class)
private String password;
private boolean ssl;
private Integer maxConnections;
private Integer prepareThreshold;
@JsonDeserialize(using = CoreCountDeserializer.class)
private Integer asyncThreads;
```

Note that database and connectionPoints can be overridden by System Vars $DATABASE and $POSTGRES, whereby $POSTGRES is
used as the connectionPoints which are pipe delimited.

```java
  "postgres-config":{
        "username":"postgres",
        "password":"password",
        "database":"${DATABASE:test}",
        "connection-points":"${POSTGRES:localhost}",
        "port":5432,
        "max-connections":"10",
        "ssl":false
        }
```

## Schema Definition

Tables are represented by annotations with the Java Pojo's. The fields in the Pojo do not need setters, and values are
set at the field level. Transients are fields that are not decorated with a `PostgresField` annotation. With views,
these are fields that do not match the java Pojo.

### PostgresTable Annotation

| Field | Description |
| ----- | ----------- |
| table | Table name. |
| version | Version of the table for Migrates. |
| schema | Schema for the table. |
| type | The annotated class, which may differ from the Pojo Class. |
| staticContent | csv or json file of static content to load. |

### PostgresField Annotation

| Field | Description |
| ----- | ----------- |
| type | Postgres data type |
| notNull | True if nulls are not allowed |
| primaryKey | True if the field is the primary key |
| populate | True if primary key is to be populated if null on insert. Needs to be of Long or String for population. |
| dateCreated | Field of type LocalDateTime to populate upon insert |
| dateUpdated | Field of type LocalDateTime to populate upon update, if `notNull` set, then this will also be populated on insert. If populate set, the dateUpdated will always be populated on update. |
| name / if the table name is different to the java field name. |
| defaultValue | Use a default value if supplied. |


### Data Types

| Type | Description | Java Type
| ----- | ----------- | ----------|
|TEXT| Variable-length character string | String |
|INTEGER| Signed four-byte integer| Integer |
|BIGINT| Signed eight-byte integer | Long |
|NUMERIC| Exact numeric of selectable precision | Double |
|BOOLEAN| Logical Boolean (true/false)| Boolean |
|TIMESTAMP| Date and time, including time zone | LocalDateTime |
|DATE| Calendar date (year, month, day)| LocalDate |
|TIME| Time of day, including time zone | LocalTime |
|JSONB| Binary JSON data, decomposed | Object |

### PostgresIndex annotation

Creates an index on the field that has the annotation.

| Field | Description |
| ----- | ----------- |
| type | Type of index BTREE by default |
| order | ASC or DESC, Ascending by default |
| unique | false by default |

### PostgresTableAccess

#### Example of Table Definition

```java

@Contract
public interface CurrencyDAO extends DOAInterface<CurrencyDO> {
}

@Service
@PostgresTable(table = "currency", type = CurrencyMixIn.class, version = CurrencyMixIn.VERSION, staticContent = "currency.csv")
public class CurrencyDAOImpl extends AbstractDAOConsumerCacheImpl<CurrencyDO, CurrencyCacheEvent> implements CurrencyDAO {

    public CurrencyDAOImpl() {
        super(CurrencyDO.class, CurrencyMixIn.class, CurrencyCacheEvent.class);
    }
}


public class CurrencyMixIn {

    private CurrencyMixIn() {
    }

    public static final int VERSION = 1;

    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true, notNull = true)
    private String code;

    @PostgresField(type = PostgresFieldType.TEXT, notNull = true)
    private String name;

    @PostgresField(type = PostgresFieldType.TEXT, notNull = true)
    private String symbol;

    @PostgresField(type = PostgresFieldType.INTEGER, notNull = true)
    private Integer decimalPlaces;
```

In the above example, a MixIn class is used instead of the DO class. The reason for this is that CurrencyDO belongs to
another module, hence could not be decorated with the `PostgresField` annotations.

## Connection

Supply the PostgresConfig to the PostgresConnection service.

```java
    @Inject
private PostgresConnection progresConnection;

        ...

        progresConnection.connect(postgresConfig);
```

## Query Params

The Query Params object is the simplest way to query the tables. If supplied as null, then the select will get all rows
from the table. Caution should be taken here, as if the table is large, it will cause an out of memory exception. It is
wise to always set a limit on the query.

Analogous to the Elastic Search, this has simple `must` and `mustNot` match queries. Must and mustNot are sets, so you
can have as many as you wish.

```java
    QueryParams params=new QueryParams();
        params.addMustNot("description","Cart Purchase");
        params.addMust("accountId","12345678");
        params.in(InQuery.of("status",TransactionStatus.COMPLETE,TransactionStatus.CANCELLED)
```

### Pagination

Pagination is controlled via the *limit* and *skip* arguments.

```java
    QueryParams params=new QueryParams();
        params.setLimit(10);
        params.setSkip(20);
```

### Order

Order is controlled via the orderBy argument, supply the javaField to sort on, order is maintained for sub-field
sorting.

```java
        QueryParams params=new QueryParams();
        params.addOrder("dateCreated");
        params.addOrder("description",PostgresOrder.DESC);
```

### Range Query

Range Queries allow for 'Between' logic. Many used for *from* a point in time, *to* another point in time. Less Than and
Greater Than can also be expressed. Range Queries also allow for Date Math, for example.

```java
    QueryParams params=new QueryParams();
        params.setOrderBy("dateCreated");
        RangeQuery rangeQuery=params.addRangeQuery("dateCreated");
        rangeQuery.setFrom(localDate).setTo("now/w");
```

For more information on
DateMath [see](https://www.elastic.co/guide/en/elasticsearch/reference/7.x/common-options.html#date-math)

### First and Last

Supplying the boolean true to the first and last query param, will return just the first and last row of the query.

### JSON

JSON can be stored in the Database by using the JSONB postgres field type. JSON values can also be queried using the
QueryParams. To access JSON values the `->>` notation is used. In the example below, the field amount is a JSON of the
following.

```java
    @JsonProperty("c")
private String currencyCode;
@JsonProperty("v")
private long value;
```

Note the cast to int as postgres needs help to cast to the correct type.

```java
    QueryParams params=new QueryParams();
        params.setLimit(50);
        params.addMust("cast(amount->>'v' as int)",100);
```

### DOAInterface

```java
public interface DOAInterface<T> {

    /**
     * Get all of the rows in the database for this table.
     * <p>
     * If the row count is large, then this may cause an OOM Exception.
     * </p>
     *
     * @return List of <T> Rows
     * @throws GUSLErrorException
     */
    List<T> getAll() throws GUSLErrorException;

    /**
     * Get rows that satisfy the query params.
     * <p>
     * If the params are null, then getAll will be called.
     * </p>
     *
     * @param params
     * @return List of <T> Rows which match the query params
     * @throws GUSLErrorException
     */
    List<T> get(QueryParams params) throws GUSLErrorException;

    /**
     * An Async get.
     *
     * @param params
     * @return
     */
    CompletableFuture<List<T>> getAsync(QueryParams params);

    /**
     * Return a row.
     *
     * @param entity to find
     * @return an Optional, which maybe empty if not found.
     * @throws GUSLErrorException
     */
    Optional<T> findRecord(T entity) throws GUSLErrorException;

    /**
     * Return a subset of the columns using a POJO view.
     * <p>
     * The field names of the view class must match the underlying <T> Object.
     * </p>
     *
     * @param clazz
     * @param params
     * @param <VIEW>
     * @return a list of subset columns that match the query params
     * @throws GUSLErrorException
     */
    <VIEW> List<VIEW> getView(Class<VIEW> clazz, QueryParams params) throws GUSLErrorException;

    /**
     * Insert an Entity into the database.
     * <p>
     * If populate, dateCreated or dateUpdated fields are present in <T> then the entity will be modified with the
     * values before insertion.
     * </p>
     *
     * @param entity to populate
     * @throws GUSLErrorException
     */
    void insert(T entity) throws GUSLErrorException;

    /**
     * Insert list of Entity as Batch Request.
     * <p>
     * As with insert, the entity will be modified if the fields are present.
     * This statement will be executed as one batch statement, if the list is large, then maybe call batchInsert in batches.
     * </p>
     *
     * @param list
     * @throws GUSLErrorException
     */
    void insertBatch(List<T> list) throws GUSLErrorException;

    /**
     * Update all fields of the row with the supplied entity.
     * <p>
     * If dateUpdated is present, that will be set upon the entity.
     * </p>
     *
     * @param entity to update
     * @throws GUSLErrorException
     */
    void update(T entity) throws GUSLErrorException;

    /**
     * Update only the fields listed in UpdateParams.
     * <p>
     * If dateUpdated is present, that will be set upon the entity.
     * </p>
     *
     * @param entity to update
     * @param params fields to update
     * @return the updated entity, as in the database
     * @throws GUSLErrorException
     */
    T update(T entity, UpdateParams params) throws GUSLErrorException;

    /**
     * Update a list of Entities as a batch Request.
     * <p>
     * As with update, the entity will be modified if the fields are present.
     * This statement will be executed as one batch statement, if the list is large, then maybe call batchUpdates in batches.
     * </p>
     *
     * @param list
     * @throws GUSLErrorException
     */
    void updateBatch(List<T> list) throws GUSLErrorException;

    /**
     * Does what it says on the tin, use with caution.
     *
     * @param entity to remove
     * @throws GUSLErrorException
     */
    void delete(T entity) throws GUSLErrorException;

    /**
     * Use with extreme caution!
     *
     * @param list
     * @throws GUSLErrorException
     */
    void deleteBatch(List<T> list) throws GUSLErrorException;
```

## Views

A view is a pojo view and not a database view, its subset of the columns (view) can be gained from the table. Views do
not need to have annotations like the DO's, but the field names need to match.

```java
public class TransactionView {

    private String id;
    private String accountId;
    // Transient as not in Transaction
    private String foo;
    private TransactionStatus status;
    private MoneyDO amount;
    private LocalDateTime dateCreated;

}
```

## Transactions

Transactions are gained from the PostgresDelete service, and then passing that to the CRUD methods so that they are all
performed within a transaction. Transactions can span tables and must be either committed or rolled back. Transactions
must be released by either committed, rollback, or voided by releasing the transaction, but the transaction must be
released.

```
    PostgresTransaction createTransaction() throws GUSLErrorException;

    void releaseTransaction(PostgresTransaction transaction, TransactionStatus status);

    void voidTransaction(PostgresTransaction transaction);

    /**
     * Commit the transaction.
     * <p>
     * If successful, and set then the commitConsumer will be called, if unsuccessful, the statements will be rollback
     * at the database level, and
     *
     * @param transaction
     * @throws GUSLErrorException
     */
    void commitTransaction(PostgresTransaction transaction) throws GUSLErrorException;

    /**
     * Rollback the transaction.
     * <p>
     * If the rollback consumer is set, call that as well.
     *
     * @param transaction
     * @throws GUSLErrorException
     */
    void rollbackTransaction(PostgresTransaction transaction) throws GUSLErrorException;
```

```java

@Inject
private PostgresDelegate thePostgresDelegate;

        PostgresTransaction transaction=thePostgresDelegate.createTransaction();

        WalletDO walletDO=new WalletDO();
        walletDO.setId("1");
        walletDO.setCurrencyCode("GBP");

        transaction.setContext(walletDO);
        transaction.setCommitConsumer(context->logger.info("Wallet Committed {}",context));
        transaction.setRollbackConsumer(context->logger.warn("Wallet failed to commit {}",context));

        walletTable.insert(walletDO,transaction);
        thePostgresDelegate.commitTransaction(transaction);
```

## Table Joins

It is possible to join two tables with a query programmatically. First, create the TableJoin and set the on and
queryParams. In the example below, markets have a corresponding event id. The below is equivalent
to `select * from market join event e on e.id = eventid where e.status = 'OPENED';`

```java
    @Inject
private MarketDAO marketTable;

@Inject
private EventDAO eventTable;


        TableJoin tableJoin=marketTable.createTableJoin(eventTable);
        QueryParams params=tableJoin.createQueryParams();
        // As this is on the event table, its qualified
        params.addMust("event.status",EventStatus.OPENED);
        params.setLimit(20);

        tableJoin.setQueryParams(params);
        // Note that these are qualified by the table prefix
        tableJoin.setOn(MatchQuery.of("event.id","market.eventId"));

        List<MarketDO> openMarkets=marketTable.executeJoin(tableJoin);
```

## Complex SQL processing.

It is possible to run any SQL via the `PostgresDelegate` and have the result mapped to a decorated POJO.

## Static Loading

Static loading of tables, is done when the table is created. The extension will decide if to load as CSV or JSON
${timestamp} will be substituted to the current date in both cases.

### CSV

```
    @PostgresTable(table = "wallet", type = WalletDO.class, staticContent = "wallet.csv", version = WalletDO.VERSION)
```

Note that the header row is the same naming convention as the POJO, as a POJO will be created for each entry, and then
inserted into the table via the batchInsert method. This also means that dateCreate and dateUpdate will be populated if
required.

```
accountId          ,currencyCode,cash     ,credit,reserve
940o64om5          ,GBP         ,5000000  ,0     ,0
940o64om6          ,GBP         ,5000000  ,0     ,0
```

When representing JSON for the JSONB field, the | (pipe) delimiter is required at the beginning and end of the JSON
string

```
kycCompleted,mobileNumber ,percentageTake
${timestamp},+447866204201,|{ "v":6 ,"d":2 }|
```

### JSON

```
    @PostgresTable(table = "geo_fence", type = GeoFenceDO.class, staticContent = "geo_fence.json", version = GeoFenceDO.VERSION)
```

The expected format for the json file is a list of the DO type.

```javascript
[
    {
        "id": "1",
        "geoFence": [
            "GI",
            "GB",
            "CA",
            "ES"
        ],
        "whiteListCidrs": [
            ...
```

## Migration

Clean, and Migration is controlled via the standalone executable `gusl.postgres.application.PostgresMigrate`.

```groovy
ext.postgressMigrateClassName = "gusl.postgres.application.PostgresMigrate"

task postgresMigrate(type: JavaExec, description: 'Performs Postgres migrate') {
    main "${postgressMigrateClassName}"
    jvmArgs = ["-DApplicationLogs=${buildDir}/logs"]
    classpath = sourceSets.main.runtimeClasspath
    args = ['-mode', getModeEnv(), '-instance', getInstanceEnv()]
}
```

### Database Backup

```
ssh user@10.10.1.10
pg_dump -h localhost -U postgres -W -F t supernova > supernova_db.tar
gzip supernova_db.tar
```

### Database Copy

```
scp user@10.10.1.10/supernova_db.tar.gz .
gunzip supernova_db.tar.gz
```

### Database Restore

```
pg_restore -h localhost -U postgres -W -F t -d supernova -c supernova_db.tar
```

If the schemas are different

```
./gradlew postgresMigrate
```



