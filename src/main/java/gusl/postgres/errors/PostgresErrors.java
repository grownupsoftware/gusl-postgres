package gusl.postgres.errors;

import gusl.core.errors.ErrorDO;
import gusl.core.exceptions.GUSLErrorException;

import java.sql.SQLException;

/**
 * @author dhudson
 * @since 15/04/2021
 */
public enum PostgresErrors {

    DRIVER_NOT_FOUND("POST01 Driver Not Found", "driver.not.found"),
    CONNECTION_FAIL("POST02 Connection Error {0}", "connection.failed"),
    PARSE_LIST_ERROR("POST03 Parse List Error {0}", "parse.list.error"),
    SQL_ERROR("POST04 SQL Error {0}", "sql.error"),
    BIND_ERROR("POST05 Bind Error {0}", "bind.error"),
    QUERY_ERROR("POST06 Query field {0} not found in {1}", "query.field.not.found"),
    RANGE_ERROR("POST07 Can't parse {0}", "range.error"),
    PARSE_SINGLE_ERROR("POST08 Parse List Error {0}", "parse.single.error"),
    PARSE_ERROR("POST09 Parse List Error {0}", "parse.error"),
    AUTO_FILL_ERROR("POST10 Unable to auto populate fields", "auto.fill.error"),
    EMPTY_QUERY_PARAMS("POST11 Empty Query Params", "empty.query.params"),
    MAX_CONNECTIONS("POST12 Max number of connections", "max.connections"),
    RESOURCE_NOT_FOUND("POST13 Resource {0} not found", "resource.not.found"),
    RECORD_NOT_FOUND("POST14 Record {0} not found", "record.not.found"),
    STATIC_DATA_ERROR("POST15 static data loading issue", "static.data.error"),
    ENCRYPTION_ERROR("POST16 encryption error", "encryption.error"),
    DEFAULT_ERROR("POST17 unable to update {0} with {1}", "default.error"),
    SECURITY_ERROR("POST18 Security Error {0}", "security.error");

    private final String message;
    private final String messageKey;

    PostgresErrors(String message, String messageKey) {
        this.message = message;
        this.messageKey = messageKey;
    }

    public ErrorDO getError() {
        return new ErrorDO(message, messageKey);
    }

    public ErrorDO getError(String... params) {
        if (params != null) {
            return new ErrorDO(null, message, messageKey, params);
        } else {
            return new ErrorDO(message, messageKey);
        }
    }

    public GUSLErrorException generateException(SQLException ex) {
        return SQL_ERROR.generateException(ex, ex.getMessage());
    }

    public GUSLErrorException generateException(String param) {
        return new GUSLErrorException(getError(param));
    }

    public GUSLErrorException generateException(String... params) {
        return new GUSLErrorException(getError(params));
    }

    public GUSLErrorException generateExceptionNoLogging(String param) {
        final ErrorDO error = getError(param);
        error.setIgnoreLogging(true);
        return new GUSLErrorException(getError(param));
    }

    public GUSLErrorException generateExceptionNoLogging(String... params) {
        final ErrorDO error = getError(params);
        error.setIgnoreLogging(true);
        return new GUSLErrorException(error);
    }

    public GUSLErrorException generateException(Throwable t, String... params) {
        return new GUSLErrorException(getError(params), t);
    }
}
