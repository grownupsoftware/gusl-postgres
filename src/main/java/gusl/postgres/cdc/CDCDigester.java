package gusl.postgres.cdc;

import gusl.postgres.application.DatabaseTable;
import lombok.CustomLog;
import org.postgresql.replication.LogSequenceNumber;
import org.postgresql.replication.PGReplicationStream;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;

/**
 * @author dhudson
 * @since 29/07/2021
 */
@CustomLog
public class CDCDigester extends Thread {

    private static final Instant PG_EPOCH = LocalDate.of(2000, 1, 1).atStartOfDay().toInstant(ZoneOffset.UTC);
    private static final byte SPACE = 32;
    private final PGReplicationStream theStream;
    private final HashMap<Integer, DatabaseTable> theTableMap;

    public CDCDigester(PGReplicationStream stream, HashMap<Integer, DatabaseTable> tableMap) {
        theStream = stream;
        theTableMap = tableMap;
    }

    public void run() {
        logger.info("Starting Digester .....");
        try {
            theStream.forceUpdateStatus();
            while (!theStream.isClosed()) {
                // Blocking
                ByteBuffer msg = theStream.read();
                processMessage(msg);

//                int offset = msg.arrayOffset();
//                byte[] source = msg.array();
//                int length = source.length - offset;
//                LogSequenceNumber lsn = theStream.getLastReceiveLSN();
//
//                String message = new String(source, offset, length);
//
//                logger.info("I have a message {}", message);
                //feedback
//                theStream.setAppliedLSN(lsn);
//                theStream.setFlushedLSN(lsn);
            }
        } catch (SQLException ex) {
            logger.warn("Unable to process message", ex);
        }

        logger.info("Digester stopped");
    }

    private void flushLsh(LogSequenceNumber lsn) throws SQLException {
        theStream.setAppliedLSN(lsn);
        theStream.setFlushedLSN(lsn);
        theStream.forceUpdateStatus();
    }

    private void processMessage(ByteBuffer buffer) throws SQLException {
        // Peek the first char
        int position = buffer.position();
        MessageType messageType = MessageType.forType((char) buffer.get());
        //buffer.position(position);

//        int offset = buffer.arrayOffset();
//        byte[] source = buffer.array();
//        int length = source.length - offset;
        LogSequenceNumber lsn = theStream.getLastReceiveLSN();

        switch (messageType) {
            case BEGIN:
                handleBeginMessage(buffer);
                break;
            case COMMIT:
                handleCommitMessage(buffer);
                break;
            case INSERT:
                decodeInsert(buffer);
                break;
            case UPDATE:
                decodeUpdate(buffer);
                break;
            case DELETE:
                decodeDelete(buffer);
                break;
            case RELATION:
            case TRUNCATE:
                logger.warn("Message Type {} skipped, not processed.", messageType);
                break;
            default:
                logger.trace("Message Type {} skipped, not processed.", messageType);
                break;
        }

        flushLsh(lsn);
    }

    private void decodeDelete(ByteBuffer buffer) {
        logger.info("Delete ..");
    }

    private void decodeUpdate(ByteBuffer buffer) {
        logger.info("Update ..");
        int relationId = buffer.getInt();

        logger.info("Looking for {}", relationId);
        DatabaseTable table = theTableMap.get(relationId);
        logger.info("Found {}", table);

        // When reading the tuple-type, we could get 3 different values, 'O', 'K', or 'N'.
        // 'O' (Optional) - States the following tuple-data is the key, only for replica identity index configs.
        // 'K' (Optional) - States the following tuple-data is the old tuple, only for replica identity full configs.
        //
        // 'N' (Not-Optional) - States the following tuple-data is the new tuple.
        // This is always present.
        //List<Column> oldColumns = null;
        char tupleType = (char) buffer.get();
        logger.info("TupleType {}", tupleType);

        if ('O' == tupleType || 'K' == tupleType) {
            resolveColumnsFromStreamTupleData(buffer, table);
            // Read the 'N' tuple type
            // This is necessary so the stream position is accurate for resolving the column tuple data
            tupleType = (char) buffer.get();
        }

        logger.info("Other Cols");
        resolveColumnsFromStreamTupleData(buffer, table);

        //List<Column> columns = resolveColumnsFromStreamTupleData(buffer, typeRegistry, table);
    }

    private void resolveColumnsFromStreamTupleData(ByteBuffer buffer, DatabaseTable table) {
        short numberOfColumns = buffer.getShort();
        for (short i = 0; i < numberOfColumns; ++i) {
            // Read the sub-message type
            // 't' : Value is represented as text
            // 'u' : An unchanged TOAST-ed value, actual value is not sent.
            // 'n' : Value is null.
            char type = (char) buffer.get();
            if (type == 't') {
                final String valueStr = readColumnValueAsString(buffer);
                logger.info("valueStr = {}", valueStr);
            } else if (type == 'u') {

            } else if (type == 'n') {
            }
        }
    }

    private void handleCommitMessage(ByteBuffer buffer) {
        logger.info("Commit ..");

        int flags = buffer.get(); // flags, currently unused
        final LogSequenceNumber lsn = LogSequenceNumber.valueOf(buffer.getLong()); // LSN of the commit
        final LogSequenceNumber endLsn = LogSequenceNumber.valueOf(buffer.getLong()); // End LSN of the transaction
        Instant commitTimestamp = PG_EPOCH.plus(buffer.getLong(), ChronoUnit.MICROS);

        logger.info("Lsn {}, End Lsn: {}, TS: {} Txn Id {}", lsn, endLsn, commitTimestamp);
    }

    private void handleBeginMessage(ByteBuffer buffer) {
        logger.info("Begin .. ");
        final LogSequenceNumber lsn = LogSequenceNumber.valueOf(buffer.getLong()); // LSN
        Instant commitTimestamp = PG_EPOCH.plus(buffer.getLong(), ChronoUnit.MICROS);
        int transactionId = buffer.getInt();

        logger.info("Lsn {}, TS: {} Txn Id {}", lsn, commitTimestamp, transactionId);
    }

    private void decodeInsert(ByteBuffer buffer) {
        logger.info("Insert ..");
    }

    private boolean skipMessage(ByteBuffer buffer) {
        // Cache position as we're going to peak at the first byte to determine message type
        // We need to reprocess all BEGIN/COMMIT messages regardless.
        int position = buffer.position();
        try {
            MessageType type = MessageType.forType((char) buffer.get());
            logger.debug("Message Type: {}", type);

            switch (type) {
                case COMMIT:
                case BEGIN:
                case RELATION:
                    // BEGIN
                    // These types should always be processed due to the nature that they provide
                    // the stream with pertinent per-transaction boundary state we will need to
                    // always cache as we potentially might reprocess the stream from an earlier
                    // LSN point.
                    //
                    // RELATION
                    // These messages are always sent with a lastReceivedLSN=0; and we need to
                    // always accept these to keep per-stream table state cached properly.
                    return false;
                default:
                    // INSERT/UPDATE/DELETE/TRUNCATE/TYPE/ORIGIN
                    // These should be excluded based on the normal behavior, delegating to default method
                    return true; //shouldMessageBeSkipped(buffer, lastReceivedLsn, startLsn, walPosition);
            }
        } finally {
            // Reset buffer position
            buffer.position(position);
        }
    }


    private String readColumnValueAsString(ByteBuffer buffer) {
        int length = buffer.getInt();
        byte[] value = new byte[length];
        buffer.get(value, 0, length);
        return new String(value, Charset.forName("UTF-8"));
    }

    private String readString(ByteBuffer buffer) {
        StringBuilder sb = new StringBuilder();
        byte b = 0;
        while ((b = buffer.get()) != 0) {
            sb.append((char) b);
        }
        return sb.toString();
    }
}
