package gusl.postgres.cdc;

import gusl.core.tostring.ToString;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 29/07/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class ReplicationSlot {

    @PostgresField(type = PostgresFieldType.TEXT, name = "slot_name")
    private String slotName;
    @PostgresField(type = PostgresFieldType.TEXT)
    private String plugin;
    @PostgresField(type = PostgresFieldType.TEXT, name = "slot_type")
    private String slotType;
    private Long datoid;
    @PostgresField(type = PostgresFieldType.TEXT)
    private String database;
    @PostgresField(type = PostgresFieldType.BOOLEAN)
    private Boolean temporary;
    @PostgresField(type = PostgresFieldType.BOOLEAN)
    private Boolean active;
    @PostgresField(type = PostgresFieldType.NUMERIC, name = "action_pid")
    private Long actionPid;
    @PostgresField(type = PostgresFieldType.CHAR_ARRAY, name = "catalog_xmin")
    private String catalogXmin;
    @PostgresField(type = PostgresFieldType.CHAR_ARRAY, name = "restart_lsn")
    private String restartLsn;
    @PostgresField(type = PostgresFieldType.CHAR_ARRAY, name = "confirmed_flush_lsn")
    private String confirmedFlushLsn;
    @PostgresField(type = PostgresFieldType.TEXT, name = "wal_status")
    private String walStatus;
    @PostgresField(type = PostgresFieldType.NUMERIC, name = "wal_safe_size")
    private Long safeWalSize;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
