package gusl.postgres.cdc;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.utils.StringUtils;
import gusl.postgres.application.DatabaseTable;
import gusl.postgres.config.PostgresConfig;
import gusl.postgres.metadata.AbstractMetadataConnection;
import gusl.postgres.metadata.CustomMetadata;
import gusl.postgres.utils.PostgresUtils;
import lombok.CustomLog;
import org.postgresql.PGConnection;
import org.postgresql.ds.PGSimpleDataSource;
import org.postgresql.jdbc.PreferQueryMode;
import org.postgresql.replication.PGReplicationStream;
import org.postgresql.replication.ReplicationSlotInfo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static gusl.core.utils.Utils.safeCollection;

/**
 * @author dhudson
 * @since 29/07/2021
 */
@CustomLog
public class ReplicationConnection extends AbstractMetadataConnection {

    // Simple decoding test_decode, limited options,
    public static final String PLUGIN_NAME = "pgoutput"; //"test_decoding";
    private static final String PUBLICATION_NAME = "supernova_publish";

    private final String SELECT_VERSION = "SELECT version()";
    private final String WAL_LEVEL = "SHOW wal_level";
    private final String GET_REPLICATION_SLOTS_SQL = "SELECT * FROM pg_replication_slots WHERE database = '%s' AND plugin = '%s'";
    private final String SELECT_PUBLICATION = "SELECT COUNT(1) FROM pg_publication WHERE pubname = '%s'";
    private final String CREATE_PUBLICATION = "CREATE PUBLICATION %s FOR ALL TABLES;";
    private final String SELECT_ROLES = "SELECT r.rolcanlogin AS rolcanlogin, r.rolreplication AS rolreplication FROM pg_roles r WHERE r.rolname = current_user;";
    private final String SELECT_OID = "SELECT relname, oid FROM pg_class;";

    private final List<String> TABLES = List.of("wallet", "currency");

    private final CustomMetadata<ReplicationSlot> slotCustomMetadata = new CustomMetadata<>(ReplicationSlot.class);
    private PGConnection pgConnection;
    private Connection connection;
    private PostgresConfig theConfig;
    private HashMap<Integer, DatabaseTable> theTableMap = new HashMap<>();

    public ReplicationConnection() {
    }

    public void connect(PostgresConfig config) throws SQLException, GUSLErrorException {
        connection = createDatasourceFor(config).getConnection();
        connection.setAutoCommit(true);
        // Make sure that the replication user exists
        boolean valid = connection.isValid(10);
        if (!valid) {
            throw new SQLException("Invalid connection details");
        }

        pgConnection = connection.unwrap(PGConnection.class);
        theConfig = config;

        List<DatabaseTable> tables = getDatabaseTables(connection);
        HashMap<String, Integer> tableIds = new HashMap<>();

        try (Statement stmt = connection.createStatement(); ResultSet rs = stmt.executeQuery(SELECT_OID)) {
            while (rs.next()) {
                tableIds.put(rs.getString(1), rs.getInt(2));
            }
        }

        // Build table map
        for (DatabaseTable table : safeCollection(tables)) {
            theTableMap.put(tableIds.get(table.getName()), table);
        }
    }

    private PGSimpleDataSource createDatasourceFor(PostgresConfig config) {
        PGSimpleDataSource dataSource = PostgresUtils.createDatasourceFor(config);
        if (StringUtils.isNotBlank(config.getReplication())) {
            // Set the minimum version for Postgres
            dataSource.setAssumeMinServerVersion("13");
            dataSource.setReplication(config.getReplication());
            dataSource.setPreferQueryMode(PreferQueryMode.SIMPLE);
        }
        return dataSource;
    }

    public void initPublication() {
        String selectPublication = String.format("SELECT COUNT(1) FROM pg_publication WHERE pubname = '%s'", PUBLICATION_NAME);
        try (Statement stmt = connection.createStatement(); ResultSet rs = stmt.executeQuery(selectPublication)) {
            if (rs.next()) {
                Long count = rs.getLong(1);
                if (count == 0L) {
                    logger.info("Creating new publication for '{}' for plugin '{}'", PUBLICATION_NAME, PLUGIN_NAME);
                    String createPublicationStmt = String.format("CREATE PUBLICATION %s FOR ALL TABLES;", PUBLICATION_NAME);
                    logger.info("Creating Publication with statement '{}'", createPublicationStmt);
                    // Publication doesn't exist, create it.
                    stmt.execute(createPublicationStmt);
                } else {
                    logger.info("A logical publication named '{}' for plugin '{}' and database '{}' is already active on the server " +
                                    "and will be used by the plugin",
                            PUBLICATION_NAME, PLUGIN_NAME, theConfig.getDatabase());
                }
            }
        } catch (SQLException ex) {
            logger.warn("Unable to initialise publication", ex);
        }
    }

    public List<ReplicationSlot> getReplicationSlots() throws SQLException {
        try {
            ResultSet rs = connection.createStatement().executeQuery(
                    String.format(GET_REPLICATION_SLOTS_SQL, theConfig.getDatabase(), PLUGIN_NAME));
            return slotCustomMetadata.parseList(rs, false);
        } catch (SQLException | GUSLErrorException ex) {
            logger.warn("Unable to get list of replication slots", ex);
        }
        return new ArrayList<>();
    }

    public ReplicationSlotInfo createReplicationSlot(String slotName) throws SQLException {
        return pgConnection.getReplicationAPI()
                .createReplicationSlot().logical()
                .withSlotName(slotName)
                .withOutputPlugin(PLUGIN_NAME)
                .withTemporaryOption()
                .make();
    }

    public PGReplicationStream createReplicationStream(String slotName) throws SQLException {
        try {
            createReplicationSlot(slotName);
        } catch (SQLException ex) {
            String msg = "ERROR: replication slot \"" + slotName + "\" already exists";
            if (!msg.equals(ex.getMessage())) {
                throw ex;
            }
            // It already exists
        }

        PGReplicationStream stream =
                pgConnection.getReplicationAPI()
                        .replicationStream()
                        .logical()
                        .withSlotName(slotName)
                        //.withSlotOption("include-xids", true)
                        //.withSlotOption("skip-empty-xacts", true)
                        //.withSlotOption("include-origin", true)
                        .withSlotOption("proto_version", 1)
                        .withSlotOption("publication_names", PUBLICATION_NAME)
                        .withStatusInterval(20, TimeUnit.SECONDS)
                        .start();

        return stream;
    }

    public HashMap<Integer, DatabaseTable> getTableMap() {
        return theTableMap;
    }
}
