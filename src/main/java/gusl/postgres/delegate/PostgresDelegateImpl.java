package gusl.postgres.delegate;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.utils.IdGenerator;
import gusl.postgres.connection.PostgresConnection;
import gusl.postgres.errors.PostgresErrors;
import gusl.postgres.metadata.CustomMetadata;
import lombok.CustomLog;
import lombok.NoArgsConstructor;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.sql.*;
import java.util.List;

/**
 * @author dhudson
 * @since 27/05/2021
 */
@Service
@CustomLog
@NoArgsConstructor
public class PostgresDelegateImpl implements PostgresDelegate {

    @Inject
    private PostgresConnection theConnection;

    @Override
    public PostgresTransaction createTransaction() throws GUSLErrorException {
        PostgresTransaction transaction = new PostgresTransaction(theConnection.getConnectionPool().getTransactionConnection());
        transaction.setTransactionId(IdGenerator.generateUniqueNodeId());
        return transaction;
    }

    @Override
    public void commitTransaction(PostgresTransaction transaction) throws GUSLErrorException {
        if (transaction == null) {
            return;
        }
        if (!transaction.isComplete()) {
            try {
                transaction.getConnection().commit();
                executeCommitConsumer(transaction);
                transaction.setStatus(TransactionStatus.COMMIT);
            } catch (SQLException ex) {
                executeRollbackConsumer(transaction);
                transaction.setStatus(TransactionStatus.ROLLBACK);
                throw PostgresErrors.SQL_ERROR.generateException(ex);
            } finally {
                releaseTransaction(transaction, transaction.getStatus());
            }
        }
    }

    @Override
    public void rollbackTransaction(PostgresTransaction transaction) throws GUSLErrorException {
        if (transaction == null) {
            return;
        }
        if (!transaction.isComplete()) {
            try {
                transaction.getConnection().rollback();
                executeRollbackConsumer(transaction);
            } catch (SQLException ex) {
                throw PostgresErrors.SQL_ERROR.generateException(ex);
            } finally {
                releaseTransaction(transaction, TransactionStatus.ROLLBACK);
            }
        }
    }

    private void executeCommitConsumer(PostgresTransaction transaction) {
        if (transaction.getCommitConsumer() != null) {
            try {
                transaction.getCommitConsumer().accept(transaction.getContext());
            } catch (Throwable t) {
                logger.warn("Error in transaction commit consumer", t);
            }
        }
    }

    private void executeRollbackConsumer(PostgresTransaction transaction) {
        if (transaction.getRollbackConsumer() != null) {
            try {
                transaction.getRollbackConsumer().accept(transaction.getContext());
            } catch (Throwable t) {
                logger.warn("Error in transaction rollback consumer", t);
            }
        }
    }

    @Override
    public void releaseTransaction(PostgresTransaction transaction, TransactionStatus status) {
        if (transaction == null) {
            return;
        }
        if (transaction.isComplete()) {
            // Done this already
            return;
        }

        theConnection.getConnectionPool().releaseTransactionConnection(transaction.getConnection());
        transaction.setStatus(status);
        transaction.setComplete();
    }

    @Override
    public void voidTransaction(PostgresTransaction transaction) {
        if (transaction == null) {
            return;
        }
        try {
            // Just in case there are any statements on it.
            transaction.getConnection().rollback();
        } catch (SQLException ignore) {

        }
        releaseTransaction(transaction, TransactionStatus.VOID);
    }

    @Override
    public boolean execute(String sql) throws GUSLErrorException {
        return internalExecute(sql, theConnection.getConnectionPool().getAutoCommitConnection());
    }

    @Override
    public boolean execute(String sql, PostgresTransaction transaction) throws GUSLErrorException {
        return internalExecute(sql, transaction.getConnection());
    }

    private boolean internalExecute(String sql, Connection connection) throws GUSLErrorException {
        try {
            try (Statement statement = connection.createStatement()) {
                return statement.execute(sql);
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public <T> List<T> get(String query, Class<T> clazz) throws GUSLErrorException {
        final CustomMetadata<T> metadata = new CustomMetadata<>(clazz);
        try (PreparedStatement statement = theConnection.createPreparedStatement(query)) {
            logger.debug("custom {}", statement);
            try (ResultSet resultSet = statement.executeQuery()) {
                return metadata.parseList(resultSet, false);
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }
}
