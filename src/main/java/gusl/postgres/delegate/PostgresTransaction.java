package gusl.postgres.delegate;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.Setter;

import java.sql.Connection;
import java.util.function.Consumer;

/**
 * @author dhudson
 * @since 07/05/2021
 */
@Getter
@Setter
public class PostgresTransaction {

    // These connections are not autoCommit
    private final Connection connection;
    private long transactionId;
    private TransactionStatus status;
    // What ever you want it to be
    private Object context;
    private Consumer<Object> commitConsumer;
    private Consumer<Object> rollbackConsumer;

    private boolean isComplete;

    PostgresTransaction(Connection connection) {
        this.connection = connection;
        status = TransactionStatus.BEGIN;
    }

    public boolean isComplete() {
        return isComplete;
    }

    void setComplete() {
        isComplete = true;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
