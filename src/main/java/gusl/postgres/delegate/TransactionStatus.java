package gusl.postgres.delegate;

/**
 * @author dhudson
 * @since 07/05/2021
 */
public enum TransactionStatus {
    BEGIN,
    ROLLBACK,
    COMMIT,
    VOID
}
