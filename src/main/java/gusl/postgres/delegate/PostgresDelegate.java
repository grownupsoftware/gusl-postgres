package gusl.postgres.delegate;

import gusl.core.exceptions.GUSLErrorException;
import org.jvnet.hk2.annotations.Contract;

import java.util.List;

/**
 * @author dhudson
 * @since 27/05/2021
 */
@Contract
public interface PostgresDelegate {

    <T> List<T> get(String query, Class<T> clazz) throws GUSLErrorException;

    boolean execute(String sql) throws GUSLErrorException;

    boolean execute(String sql, PostgresTransaction transaction) throws GUSLErrorException;

    PostgresTransaction createTransaction() throws GUSLErrorException;

    void releaseTransaction(PostgresTransaction transaction, TransactionStatus status);

    void voidTransaction(PostgresTransaction transaction);

    void commitTransaction(PostgresTransaction transaction) throws GUSLErrorException;

    void rollbackTransaction(PostgresTransaction transaction) throws GUSLErrorException;
}

