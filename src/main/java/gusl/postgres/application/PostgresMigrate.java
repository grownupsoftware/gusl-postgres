package gusl.postgres.application;

import gusl.core.utils.IdGenerator;
import gusl.core.utils.StringUtils;
import gusl.core.utils.SystemPropertyUtils;
import gusl.postgres.config.NodeConfigUtils;
import gusl.postgres.config.NodePostgresConfig;
import gusl.postgres.config.PostgresConfig;
import gusl.postgres.exceptions.PostgresEvolutionException;
import lombok.CustomLog;
import org.apache.commons.cli.*;

import java.io.File;
import java.io.IOException;

/**
 * @author dhudson
 * @since 14/04/2021
 */
@CustomLog
public class PostgresMigrate {

    private static final String HELP_ARG = "h";
    private static final String DEBUG_ARG = "debug";
    private static final String CLEAN_ARG = "clean";
    private static final String MODE_ARG = "mode";
    private static final String INSTANCE_ARG = "instance";
    private static final String RERUN_INSTANCE_ARG = "rerun_instance";
    private static final String TABLE = "table";
    private static final String LOAD = "load";

    private void printHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("postgres-migrate", options);
    }

    private void migrate(MigrateConfig migrateConfig) throws IOException, PostgresEvolutionException {

        if (SystemPropertyUtils.hasConfigLocation()) {
            if (SystemPropertyUtils.getGivenConfig().toLowerCase().contains("prod")) {
                logger.info("--- Production Instance  --- ");
                if (migrateConfig.isClean()) {
                    logger.warn("Can't clean a PRODUCTION database .........");
                    return;
                }
            }
        }

        NodePostgresConfig nodePostgresConfig = NodeConfigUtils.loadConfig(NodePostgresConfig.class);
        PostgresConfig config = nodePostgresConfig.getPostgresConfig();

        logger.info("Postgres Config {}, Migrate Config {}", config, migrateConfig);

        PostgresEvolve evolve = new PostgresEvolve(config, migrateConfig);
        evolve.evolve();
    }

    public void setup(String... args) {
        try {
            Options options = new Options();
            options.addOption(DEBUG_ARG, "Enable debugging");
            options.addOption(HELP_ARG, "Help");
            options.addOption(CLEAN_ARG, "Clean the keyspace");
            options.addOption(MODE_ARG, true, "Application Operations mode");
            options.addOption(INSTANCE_ARG, true, "Application Instance e.g. uk, ie or com");
            options.addOption(RERUN_INSTANCE_ARG, "Run the instance scripts regardless");
            options.addOption(LOAD, true, "Load data from the given location");
            options.addOption(TABLE, true, "Just for the given table");
            CommandLineParser parser = new DefaultParser();
            CommandLine commandLine;

            try {
                commandLine = parser.parse(options, args);
            } catch (ParseException ex) {
                logger.error("Command Line Exception " + ex.getLocalizedMessage());
                printHelp(options);
                return;
            }

            if (commandLine.hasOption(HELP_ARG)) {
                System.err.println("No arguments supplied");
                printHelp(options);
                return;
            }

            MigrateConfig migrateConfig = new MigrateConfig();

            if (commandLine.hasOption(CLEAN_ARG)) {
                migrateConfig.setClean(true);
            }

            if (commandLine.hasOption(MODE_ARG)) {
                migrateConfig.setMode(commandLine.getOptionValue(MODE_ARG));
            }

            if (commandLine.hasOption(INSTANCE_ARG)) {
                migrateConfig.setInstance(commandLine.getOptionValue(INSTANCE_ARG));
            }

            if (commandLine.hasOption(RERUN_INSTANCE_ARG)) {
                migrateConfig.setRunInstanceScripts(true);
            }

            if (commandLine.hasOption(LOAD)) {
                migrateConfig.setLoad(true);
                migrateConfig.setLoadPath(commandLine.getOptionValue(LOAD));
            }

            if (commandLine.hasOption(TABLE)) {
                migrateConfig.setTable(commandLine.getOptionValue(TABLE));
                if (StringUtils.isBlank(migrateConfig.getTable())) {
                    throw new PostgresEvolutionException("Missing table name");
                }
            }

            IdGenerator.setIfNotSet();

            migrate(migrateConfig);

        } catch (IOException | PostgresEvolutionException ex) {
            System.err.println("Unable to perform migrate " + ex.getLocalizedMessage());
            logger.warn("Unable to perform migrate {}", ex);
        }

    }

    public static void main(String... args) {
        // Let's create the Log Directory before we do anything
        String logDir = SystemPropertyUtils.getLogBase();
        if (logDir != null) {
            File file = new File(logDir);
            file.mkdirs();
        } else {
            logger.warn("System Property {} not set", SystemPropertyUtils.LOGS_LOCATION_KEY);
        }

        PostgresMigrate migrate = new PostgresMigrate();
        migrate.setup(args);

    }
}
