package gusl.postgres.application;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.Setter;

/**
 * @author dhudson
 * @since 05/05/2021
 */
@Getter
@Setter
public class DatabaseColumn {
    private String name;
    private int type;
    private String typeName;
    private int nullable;
    private String isNullable;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
