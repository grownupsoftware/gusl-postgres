package gusl.postgres.application;

import gusl.postgres.dao.AbstractDAOPostgresImpl;
import gusl.postgres.model.PostgresTable;
import lombok.Getter;

/**
 * @author dhudson
 * @since 03/10/2022
 */
@Getter
public class LoadHelper {

    private final PostgresTable table;
    private final Class<? extends AbstractDAOPostgresImpl> access;
    private final String tableName;

    LoadHelper(Class<?> doa) {
        access = (Class<? extends AbstractDAOPostgresImpl>) doa;
        table = access.getAnnotation(PostgresTable.class);
        tableName = table.table();
    }
}
