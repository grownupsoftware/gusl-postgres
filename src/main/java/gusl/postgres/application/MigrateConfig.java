package gusl.postgres.application;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 15/04/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class MigrateConfig {

    private boolean load;
    private String loadPath;
    private boolean clean;
    private String mode;
    private String instance;
    private boolean runInstanceScripts;
    private String table;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
