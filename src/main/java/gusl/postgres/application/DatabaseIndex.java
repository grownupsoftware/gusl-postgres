package gusl.postgres.application;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.Setter;

/**
 * @author dhudson
 * @since 05/05/2021
 */
@Getter
@Setter
public class DatabaseIndex {

    private String name;
    private String columnName;
    private String order;
    private boolean unique;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
