package gusl.postgres.application;

import gusl.core.tostring.ToString;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import gusl.postgres.model.PostgresTable;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author dhudson
 * @since 15/04/2021
 */
@Getter
@Setter
public class EvolutionDO {
    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true, notNull = true)
    private String name;
    @PostgresField(type = PostgresFieldType.TIMESTAMP, notNull = true, dateCreated = true)
    private LocalDateTime created;
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateUpdated = true)
    private LocalDateTime modified;
    @PostgresField(type = PostgresFieldType.INTEGER)
    private Integer version;

    public EvolutionDO() {
    }

    public EvolutionDO(String tableName) {
        name = tableName;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
