package gusl.postgres.application;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 05/05/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class DatabaseTable {

    private String catalog;
    private String schema;
    private String name;
    private String type;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
