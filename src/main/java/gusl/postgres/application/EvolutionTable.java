package gusl.postgres.application;

import gusl.postgres.dao.AbstractDAOPostgresImpl;
import gusl.postgres.model.PostgresTable;

/**
 * @author dhudson
 * @since 16/04/2021
 */
@PostgresTable(table = "evolutions", type = EvolutionDO.class, version = 1)
public class EvolutionTable extends AbstractDAOPostgresImpl<EvolutionDO> {

    public EvolutionTable() {
        super(EvolutionDO.class);
    }

}
