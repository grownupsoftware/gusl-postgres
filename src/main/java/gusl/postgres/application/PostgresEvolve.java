package gusl.postgres.application;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.utils.ClassUtils;
import gusl.core.utils.IOUtils;
import gusl.core.utils.StringUtils;
import gusl.postgres.config.PostgresConfig;
import gusl.postgres.connection.PostgresConnection;
import gusl.postgres.connection.PostgresConnectionImpl;
import gusl.postgres.dao.AbstractDAOPostgresImpl;
import gusl.postgres.dao.DAOPostgresInterface;
import gusl.postgres.exceptions.PostgresEvolutionException;
import gusl.postgres.model.*;
import gusl.postgres.utils.PostgresClassUtils;
import gusl.postgres.utils.PostgresUtils;
import gusl.query.UpdateParams;
import lombok.CustomLog;
import lombok.NonNull;
import lombok.SneakyThrows;

import java.io.File;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 * @author dhudson
 * @since 15/04/2021
 */
@CustomLog
public class PostgresEvolve {

    public static final String MODE_BASE_DIRECTORY = "postgres/mode/";
    public static final String INSTANCE_BASE_DIRECTORY = "postgres/instance/";
    public static final String STATIC_PATTERN = "_static_";

    private static final String KILL_ACTIVE_CONNECTIONS = "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid()";

    // https://jdbc.postgresql.org/documentation/head/replication.html

    private static final String LIST_DATABASES_SQL = "select datname from pg_database where datistemplate = false;";

    private final PostgresConfig thePostgresConfig;
    private final MigrateConfig theMigrateConfig;

    private EvolutionTable theEvolutionTable = new EvolutionTable();

    public PostgresEvolve(PostgresConfig postgresConfig, MigrateConfig migrateConfig) {
        thePostgresConfig = postgresConfig;
        theMigrateConfig = migrateConfig;
    }

    public void evolve() throws PostgresEvolutionException {
        try {
            logger.info("Migration Starting");
            // This should create a database and connect to it
            checkDatabaseExists();

            PostgresConnection connection = new PostgresConnectionImpl();
            connection.connect(thePostgresConfig);
            Statement statement = connection.createStatement();

            if (theMigrateConfig.isLoad()) {
                loadStaticContent(connection, theMigrateConfig.getLoadPath());
                return;
            }

            theEvolutionTable.setConnection(connection);
            theEvolutionTable.config();

            Map<String, DatabaseTable> tableMap = new HashMap<>();
            connection.getDatabaseTables().forEach(databaseTable -> {
                tableMap.put(databaseTable.getName(), databaseTable);
            });

            // Let's make sure that the Evo Table exists
            PostgresTable evoTableAnnotation = EvolutionTable.class.getAnnotation(PostgresTable.class);
            if (!tableMap.containsKey(evoTableAnnotation.table())) {
                createTable(EvolutionDO.class, evoTableAnnotation, statement);
            }

            Set<Class<?>> classes = PostgresClassUtils.getAnnotatedClasses(PostgresTable.class);
            for (Class<?> clazz : classes) {
                PostgresTable tableAnnotation = clazz.getAnnotation(PostgresTable.class);
                if (tableAnnotation.table().equals(evoTableAnnotation.table())) {
                    continue;
                }

                Optional<EvolutionDO> maybe = theEvolutionTable.findRecord(new EvolutionDO(tableAnnotation.table()));

                if (StringUtils.isNotBlank(theMigrateConfig.getTable())) {
                    if (!theMigrateConfig.getTable().equals(tableAnnotation.table())) {
                        // Only interested in one table ..
                        continue;
                    }
                }

                if (maybe.isEmpty()) {
                    createTable(tableAnnotation.type(), tableAnnotation, statement);
                    loadStaticContent(clazz, tableAnnotation, connection);
                    continue;
                }

                if (theMigrateConfig.isClean()) {
                    // Drop the table and then reload it
                    logger.info("Dropping table {}", tableAnnotation.table());
                    statement.executeUpdate("drop table if exists " + tableAnnotation.table());
                    theEvolutionTable.delete(createFrom(tableAnnotation));
                    createTable(tableAnnotation.type(), tableAnnotation, statement);
                    loadStaticContent(clazz, tableAnnotation, connection);
                    continue;
                }

                EvolutionDO existing = maybe.get();
                DatabaseTable table = tableMap.get(tableAnnotation.table());
                if (existing.getVersion().intValue() != tableAnnotation.version()) {
                    logger.info("Its time for an upgrade ..  Lets see if there are new fields");
                    Map<String, DatabaseColumn> columnMap = new HashMap<>();
                    connection.getDatabaseColumns(table).forEach(databaseColumn -> {
                        columnMap.put(databaseColumn.getName(), databaseColumn);
                    });

                    List<Field> indexes = new ArrayList<>(3);
                    List<Field> colsToAdd = new ArrayList<>();
                    for (Field field : PostgresClassUtils.getDataFieldsFor(tableAnnotation.type())) {
                        PostgresField postgresField = field.getAnnotation(PostgresField.class);
                        if (postgresField == null) {
                            // Transient field
                            continue;
                        }

                        PostgresIndex index = field.getAnnotation(PostgresIndex.class);
                        if (index != null) {
                            indexes.add(field);
                        }

                        if (!columnMap.containsKey(field.getName().toLowerCase())) {
                            // Need to add this column
                            colsToAdd.add(field);
                        }
                    }

                    // Might be an index added
                    if (!colsToAdd.isEmpty()) {

                        for (Field field : colsToAdd) {
                            PostgresField postgresField = field.getAnnotation(PostgresField.class);
                            StringBuilder builder = new StringBuilder(128);
                            builder.append("ALTER TABLE ");
                            builder.append(tableAnnotation.table());
                            builder.append(" ADD ");

                            builder.append(createFieldDefinition(field, postgresField));

                            builder.append(";");
                            logger.info("Alter {}", builder.toString());
                            statement.executeUpdate(builder.toString());
                        }
                    }

                    if (!indexes.isEmpty()) {
                        // Let's check for index change ..
                        Map<String, DatabaseIndex> indexMap = new HashMap<>();
                        connection.getIndices(table).forEach(databaseIndex -> {
                            indexMap.put(databaseIndex.getColumnName(), databaseIndex);
                        });
                        for (Field indexField : indexes) {
                            if (!indexMap.containsKey(indexField.getName().toLowerCase())) {
                                // Create Index
                                createIndex(indexField.getAnnotation(PostgresIndex.class), indexField, tableAnnotation, statement);
                            }
                        }
                    }

                    updateEvolution(tableAnnotation);
                }
            }

            // Views ...
            viewMigrate(statement);
        } catch (SQLException | GUSLErrorException ex) {
            logger.warn("Evolve Exception", ex);
            throw new PostgresEvolutionException(ex.getMessage(), ex);
        } catch (Throwable t) {
            logger.warn("Evolve Exception", t);
            throw new PostgresEvolutionException(t.getMessage(), t);
        }

        logger.info("Migration Complete");
    }

    private void viewMigrate(Statement statement) throws PostgresEvolutionException, SQLException, GUSLErrorException {
        Set<Class<?>> classes = PostgresClassUtils.getAnnotatedClasses(PostgresView.class);

        for (Class<?> clazzView : classes) {
            PostgresView viewAnnotation = clazzView.getAnnotation(PostgresView.class);

            if (!viewAnnotation.isDbView()) {
                continue;
            }

            if (StringUtils.isBlank(viewAnnotation.name())) {
                throw new PostgresEvolutionException("Missing view name " + viewAnnotation);
            }

            Optional<EvolutionDO> maybe = theEvolutionTable.findRecord(new EvolutionDO(viewAnnotation.name()));

            if (StringUtils.isNotBlank(theMigrateConfig.getTable())) {
                if (!theMigrateConfig.getTable().equals(viewAnnotation.name())) {
                    // Only interested in one table ..
                    continue;
                }
            }

            if (maybe.isEmpty()) {
                createView(viewAnnotation, statement);
                continue;
            }

            if (theMigrateConfig.isClean()) {
                // Drop the table and then reload it
                logger.info("Dropping view {}", viewAnnotation.name());
                statement.executeUpdate("drop view if exists " + viewAnnotation.name());
                theEvolutionTable.delete(createFrom(viewAnnotation));
                createView(viewAnnotation, statement);
                continue;
            }
            EvolutionDO existing = maybe.get();
            if (existing.getVersion() != viewAnnotation.version()) {
                logger.info("Its time for an upgrade ..  ");
                logger.info("Dropping view {}", viewAnnotation.name());
                statement.executeUpdate("drop view if exists " + viewAnnotation.name());
                theEvolutionTable.delete(createFrom(viewAnnotation));
                createView(viewAnnotation, statement);
                updateEvolution(viewAnnotation);
            }
        }
    }

    private void loadStaticContent(PostgresConnection connection, String location) {
        try {
            File fileLocation = new File(location);
            if (!fileLocation.exists()) {
                logger.warn("Location {} doesn't exist", location);
                return;
            }

            List<File> filesToLoad;

            if (fileLocation.isFile()) {
                filesToLoad = Arrays.asList(new File(location));
            } else {
                filesToLoad = new ArrayList<>();
                filesToLoad.addAll(Arrays.asList(fileLocation.listFiles()));
            }

            logger.info("Resources to Load {}", filesToLoad);

            Map<String, LoadHelper> loadHelpers = new HashMap<>();
            for (Class<?> doa : PostgresClassUtils.getAnnotatedClasses(PostgresTable.class)) {
                LoadHelper helper = new LoadHelper(doa);
                loadHelpers.put(helper.getTableName(), helper);
            }

            for (File resource : filesToLoad) {
                logger.info("Resource {}", resource);
                String name = resource.getName();
                LoadHelper helper = loadHelpers.get(StringUtils.getBaseName(name));
                if (helper == null) {
                    logger.warn("Can't find table for {}", resource.getName());
                } else {
                    loadData(helper.getAccess(), helper.getTable(), connection, resource);
                }
            }
        } catch (Throwable ex) {
            logger.warn("Unable to locate {}", location, ex);
        }
    }

    private void loadStaticContent(Class<?> clazzImpl, PostgresTable tableAnnotation, PostgresConnection connection) throws GUSLErrorException {
        if (StringUtils.isBlank(tableAnnotation.staticContent())) {
            // Nothing to do
            return;
        }

        try {
            String resourceToLoad = "postgres/instance/com/" + tableAnnotation.staticContent();
            loadData(clazzImpl, tableAnnotation, connection, IOUtils.getResourceAsFile(resourceToLoad, PostgresEvolve.class.getClassLoader()));
        } catch (Throwable ex) {
            logger.warn("Unable to load static content for {}", tableAnnotation.table(), ex);
        }
    }

    @SneakyThrows
    private void loadData(Class<?> clazzImpl, PostgresTable tableAnnotation, PostgresConnection connection, File resource) throws GUSLErrorException {
        DAOPostgresInterface tableAccess = (DAOPostgresInterface) ClassUtils.getNewInstance(clazzImpl);
        AbstractDAOPostgresImpl dao = (AbstractDAOPostgresImpl) tableAccess;

        dao.setConnection(connection);
        dao.config();
        int rowsLoaded = dao.loadFile(resource);
        logger.info("Loaded {} rows into {},", rowsLoaded, tableAnnotation.table());
    }

    private void updateEvolution(PostgresTable table) throws GUSLErrorException {
        theEvolutionTable.update(createFrom(table), new UpdateParams().addUpdateField("version"));
    }

    private void updateEvolution(PostgresView viewAnnotation) throws GUSLErrorException {
        theEvolutionTable.update(createFrom(viewAnnotation), new UpdateParams().addUpdateField("version"));
    }

    private void insertIntoEvo(PostgresView table) throws GUSLErrorException {
        theEvolutionTable.insert(createFrom(table));
    }

    private void insertIntoEvo(PostgresTable table) throws GUSLErrorException {
        theEvolutionTable.insert(createFrom(table));
    }

    private EvolutionDO createFrom(PostgresTable table) {
        EvolutionDO evo = new EvolutionDO();
        evo.setVersion(table.version());
        evo.setName(table.table());
        return evo;
    }

    private EvolutionDO createFrom(PostgresView view) {
        EvolutionDO evo = new EvolutionDO();
        evo.setVersion(view.version());
        evo.setName(view.name());
        return evo;
    }

    private void createTable(Class<?> clazz, @NonNull PostgresTable tableAnnotation, Statement statement)
            throws SQLException, GUSLErrorException {

        logger.info("Creating table {}", tableAnnotation.table());

        // Time to create the table
        StringBuilder builder = new StringBuilder(128);
        builder.append("CREATE TABLE ");
        builder.append(tableAnnotation.table());
        builder.append(" (");

        Set<Field> fields = PostgresClassUtils.getDataFieldsFor(clazz);
        boolean first = true;
        for (Field field : fields) {
            PostgresField postgresField = field.getAnnotation(PostgresField.class);
            if (postgresField == null) {
                continue;
            }

            if (!first) {
                builder.append(", ");
            }

            builder.append(createFieldDefinition(field, postgresField));

            first = false;
        }

        builder.append(");");

        logger.info("Executing ... {}", builder.toString());
        statement.execute(builder.toString());

        // Do we have any indices?
        for (Field field : fields) {
            PostgresIndex postgresIndex = field.getAnnotation(PostgresIndex.class);
            if (postgresIndex != null) {
                createIndex(postgresIndex, field, tableAnnotation, statement);
            }
        }

        insertIntoEvo(tableAnnotation);
    }

    private void createView(@NonNull PostgresView viewAnnotation, Statement statement)
            throws SQLException, GUSLErrorException {

        logger.info("Creating view {}", viewAnnotation.name());

        StringBuilder builder = new StringBuilder(128);
        builder.append("CREATE VIEW ");
        builder.append(viewAnnotation.name());
        builder.append(" AS ");
        builder.append(viewAnnotation.sql());
        builder.append(";");

        logger.info("Executing ... {}", builder.toString());
        statement.execute(builder.toString());
        insertIntoEvo(viewAnnotation);
    }

    private void checkDatabaseExists() throws SQLException, GUSLErrorException {
        // Don't connect to a DB at this point
        Connection connection = PostgresUtils.connect(thePostgresConfig, false);
        ResultSet rs = connection.prepareStatement(LIST_DATABASES_SQL).executeQuery();
        HashSet databases = new HashSet();

        while (rs.next()) {
            databases.add(rs.getString(1).toLowerCase());
        }
        rs.close();

        if (!databases.contains(thePostgresConfig.getDatabase().toLowerCase())) {
            Statement statement = connection.createStatement();
            statement.execute("CREATE DATABASE " + thePostgresConfig.getDatabase() + ";");
            connection.close();
        }
    }

    private void createIndex(PostgresIndex index, Field field, PostgresTable table, Statement statement) throws SQLException {
        StringBuilder builder = new StringBuilder(64);
        builder.append("CREATE ");
        if (index.unique()) {
            builder.append("UNIQUE ");
        }
        builder.append("INDEX idx_");
        builder.append(table.table());
        builder.append("_");
        builder.append(field.getName());
        builder.append(" ON ");
        builder.append(table.table());
        builder.append(" USING ");
        builder.append(index.type());
        builder.append(" (");
        builder.append(field.getName());
        builder.append(" ");
        builder.append(index.order());
        builder.append(" );");
        logger.info("Executing ... {}", builder.toString());
        statement.execute(builder.toString());
    }

    private String createFieldDefinition(Field javaField, PostgresField postgresField) {
        StringBuilder builder = new StringBuilder(64);
        if (StringUtils.isNotBlank(postgresField.name())) {
            builder.append(postgresField.name());
        } else {
            builder.append(javaField.getName());
        }
        builder.append(" ");
        switch (postgresField.type()) {
            case ZONED_TIMESTAMP:
            case LIST:
                builder.append(PostgresFieldType.JSONB.name());
                break;
            default:
                builder.append(postgresField.type().name());
        }

        if (postgresField.notNull() || postgresField.primaryKey()) {
            builder.append(" NOT NULL");
        }
        if (postgresField.primaryKey()) {
            builder.append(" PRIMARY KEY");
        }
        return builder.toString();
    }
}
