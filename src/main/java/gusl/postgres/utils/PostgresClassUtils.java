package gusl.postgres.utils;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import gusl.core.utils.ClassUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Set;

/**
 * @author dhudson on 2019-08-15
 */
public class PostgresClassUtils {

    private static final Cache<String, Set<Field>> theFieldsCache = CacheBuilder.newBuilder().maximumSize(100).build();

    private PostgresClassUtils() {
    }

    public static synchronized Set<Class<?>> getAnnotatedClasses(Class<? extends Annotation> annotatedClass) {
        return ClassUtils.getAnnotatedClasses(annotatedClass);
    }

    public static Set<Field> getDataFieldsFor(Class<?> clazz) {
        Set<Field> fields = theFieldsCache.getIfPresent(clazz.getName());
        if (fields == null) {

            fields = ClassUtils.getFieldsFor(clazz, true);
            theFieldsCache.put(clazz.getName(), fields);
        }

        return fields;
    }
}
