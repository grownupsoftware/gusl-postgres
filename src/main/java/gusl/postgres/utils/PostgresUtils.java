package gusl.postgres.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.dates.DateMathParser;
import gusl.core.dates.GUSLDateException;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.security.InvalidOperationException;
import gusl.core.security.ObfuscateMethod;
import gusl.core.security.ObfuscatedStorage;
import gusl.core.security.PasswordStorage;
import gusl.core.time.GUSLZonedDateTime;
import gusl.core.time.TimeService;
import gusl.core.utils.Platform;
import gusl.core.utils.Utils;
import gusl.postgres.config.PostgresConfig;
import gusl.postgres.errors.PostgresErrors;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import gusl.postgres.model.PostgresTable;
import lombok.NonNull;
import org.postgresql.PGStatement;
import org.postgresql.ds.PGSimpleDataSource;
import org.postgresql.util.PGobject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * https://jdbc.postgresql.org/documentation/head/load.html
 *
 * @author dhudson
 * @since 15/04/2021
 */
public class PostgresUtils {

    public static final ObjectMapper OBJECT_MAPPER = ObjectMapperFactory.getDefaultObjectMapper();
    public static final ZoneId ZONE_ID = TimeService.UTC.toZoneId();

    public static Connection connect(PostgresConfig config, boolean database) throws GUSLErrorException {
        try {
            Class.forName("org.postgresql.Driver");
            PGSimpleDataSource dataSource = createDatasourceFor(config);
            if (!database) {
                dataSource.setDatabaseName(null);
            }

            Connection connection = dataSource.getConnection();
            connection.setAutoCommit(true);
            return connection;
        } catch (ClassNotFoundException ex) {
            throw PostgresErrors.DRIVER_NOT_FOUND.generateException(ex);
        } catch (SQLException ex) {
            throw PostgresErrors.CONNECTION_FAIL.generateException(ex, config.getConnectionPoints());
        }
    }

    public static PGSimpleDataSource createDatasourceFor(PostgresConfig config) {
        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        //dataSource.setApplicationName("Supernova");
        dataSource.setTcpKeepAlive(true);
        dataSource.setDatabaseName(config.getDatabase());
        dataSource.setUser(config.getUsername());
        dataSource.setPassword(config.getPassword());
        dataSource.setServerNames(config.getServerNames());

        if (config.getPrepareThreshold() != null) {
            dataSource.setPrepareThreshold(config.getPrepareThreshold());
        }

        if (config.isSsl()) {
            dataSource.setSslMode("require");
            dataSource.setSslfactory("org.postgresql.ssl.NonValidatingFactory");
        }

        return dataSource;
    }

    public static Connection connect(PostgresConfig config) throws GUSLErrorException {
        return connect(config, true);
    }

    public static String dump(ResultSet rs) throws SQLException {
        if (!rs.isBeforeFirst()) {
            return "no rows found";
        } else {
            StringBuilder builder = new StringBuilder(128);
            builder.append("types:");
            builder.append(Platform.LINE_SEPARATOR);

            for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
                builder.append("\t");
                builder.append(rs.getMetaData().getColumnName(i + 1));
                builder.append(" = ");
                builder.append(rs.getMetaData().getColumnTypeName(i + 1));
                builder.append(Platform.LINE_SEPARATOR);
            }

            builder.append("values:");
            builder.append(Platform.LINE_SEPARATOR);

            while (rs.next()) {
                for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
                    builder.append(rs.getMetaData().getColumnName(i));
                    builder.append(" = ");
                    builder.append(rs.getObject(i));
                    builder.append(Platform.LINE_SEPARATOR);
                }
            }

            return builder.toString();
        }
    }

    public static <T> String createInsertStatement(@NonNull Class<T> clazz, List<Field> javaFields, List<PostgresField> postgresFields) {
        PostgresTable table = clazz.getAnnotation(PostgresTable.class);
        if (table == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(128);
        Set<Field> fields = PostgresClassUtils.getDataFieldsFor(clazz);
        builder.append("INSERT INTO ");
        builder.append(table.table());
        builder.append(" (");
        StringBuilder values = new StringBuilder(128);
        values.append("VALUES (");
        boolean isFirst = true;
        for (Field field : fields) {
            if (!isFirst) {
                builder.append(",");
                values.append(",");
            }

            builder.append(field.getName());
            PostgresField postgresField = field.getAnnotation(PostgresField.class);

            values.append("?");
            if (postgresField.type() == PostgresFieldType.JSONB) {
                values.append("::JSONB");
            }

            isFirst = false;
        }

        builder.append(") ");
        builder.append(values);
        builder.append(");");
        return builder.toString();
    }

    public static void setPrepareThreshold(PreparedStatement statement, int threshold) throws SQLException {
        PGStatement unwrapStatement = statement.unwrap(PGStatement.class);
        unwrapStatement.setPrepareThreshold(threshold);
    }

    public static void bindStatementValue(PreparedStatement statement, Field javaField, PostgresField postgresField, int sqlIndex, Object value) throws GUSLErrorException {
        bindStatementValue(statement, javaField, postgresField, sqlIndex, value, false);
    }

    public static void bindStatementValue(PreparedStatement statement, Field javaField, PostgresField postgresField, int sqlIndex, Object value, boolean caseInsensitive) throws GUSLErrorException {
        try {

            if (value == null) {
                statement.setObject(sqlIndex, value);
                return;
            }

            if (javaField.getType().isEnum()) {
                statement.setString(sqlIndex, value.toString());
                return;
            }

            switch (postgresField.type()) {
                case ZONED_TIMESTAMP:
                    GUSLZonedDateTime guslZonedDateTime = new GUSLZonedDateTime((ZonedDateTime) value);
                    statement.setObject(sqlIndex, createPGObject(guslZonedDateTime));
                    break;
                case LIST:
                case JSONB:
                    statement.setObject(sqlIndex, createPGObject(value));
                    break;
                case TIMESTAMP:
                case DATE:
                    statement.setObject(sqlIndex, parseDate(value, ZONE_ID));
                    break;
                case TEXT:
                    if (postgresField.securityMethod() != ObfuscateMethod.NONE) {
                        if (caseInsensitive) {
                            statement.setString(sqlIndex, getStringValue(postgresField, value).toLowerCase());
                        } else {
                            statement.setString(sqlIndex, getStringValue(postgresField, value));
                        }
                    } else {
                        statement.setObject(sqlIndex, value);
                    }
                    break;
                case BOOLEAN:
                    statement.setBoolean(sqlIndex, Boolean.parseBoolean(value.toString()));
                    break;
//                case INTEGER:
//                    statement.setInt(sqlIndex, (Integer) value);
//                    break;
//                case BIGINT:
//                    statement.setBigDecimal(sqlIndex, BigDecimal.valueOf((Long) value));
//                    break;
//                case NUMERIC:
//                    statement.setDouble(sqlIndex, (Double) value);
//                    break;
                default:
                    statement.setObject(sqlIndex, value);
            }
        } catch (SQLException ex) {
            clearQuietly(statement);
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        } catch (IOException ex) {
            throw PostgresErrors.BIND_ERROR.generateException(ex, ex.getLocalizedMessage());
        } catch (InvalidOperationException ex) {
            throw PostgresErrors.ENCRYPTION_ERROR.generateException(ex);
        }
    }

    private static PGobject createPGObject(Object value) throws IOException, SQLException {
        PGobject obj = new PGobject();
        obj.setType("json");
        obj.setValue(OBJECT_MAPPER.writeValueAsString(value));
        return obj;
    }

    public static Object getObjectValue(PostgresField postgresField, String value) throws GUSLErrorException {
        try {
            switch (postgresField.type()) {
                case TEXT:
                    return value;
                case BOOLEAN:
                    return Boolean.parseBoolean(value);
                case NUMERIC:
                    return BigDecimal.valueOf(Double.valueOf(value));
            }
        } catch (Throwable t) {
            throw PostgresErrors.DEFAULT_ERROR.generateException(postgresField.name(), value);
        }
        // Don't know hwo to handle that..
        throw PostgresErrors.DEFAULT_ERROR.generateException(postgresField.name(), value);
    }

    private static String getStringValue(PostgresField postgresField, Object value) throws InvalidOperationException {
        String str = value.toString();
        if (postgresField.securityMethod() == ObfuscateMethod.NONE) {
            return str;
        }
        switch (postgresField.securityMethod()) {
            case SALTED_WRITE:
                if (!PasswordStorage.isHashed(str)) {
                    str = PasswordStorage.createHash(str);
                }
                break;
            case OBFUSCATED_WRITE:
            case OBFUSCATED_WRITE_READ:
                if (!ObfuscatedStorage.isObfuscated(str)) {
                    str = ObfuscatedStorage.encrypt(str);
                }
                break;
        }

        return str;
    }

    public static void clearQuietly(PreparedStatement statement) {
        try {
            statement.clearParameters();
            statement.clearBatch();
            statement.clearWarnings();
        } catch (SQLException ignore) {
            // ignore
        }
    }

    public static LocalDateTime parseDate(@NonNull Object value, ZoneId zoneId) throws GUSLErrorException {
        if (value instanceof LocalDateTime) {
            return (LocalDateTime) value;
        }
        if (value instanceof Date) {
            return Utils.asLocalDateTime((Date) value, zoneId);
        }
        if (value instanceof Instant) {
            return LocalDateTime.ofInstant((Instant) value, zoneId);
        }
        if (value instanceof LocalDate) {
            return ((LocalDate) value).atStartOfDay();
        }
        if (value instanceof String) {
            try {
                return LocalDateTime.ofInstant(DateMathParser.parse((String) value), zoneId);
            } catch (GUSLDateException ex) {
                throw PostgresErrors.RANGE_ERROR.generateException(value.toString());
            }
        }
        if (value instanceof ZonedDateTime) {
            return ((ZonedDateTime) value).toLocalDateTime();
        }
        //
        throw PostgresErrors.RANGE_ERROR.generateException(value.toString());
    }

    public static LocalDateTime now() {
        return LocalDateTime.now(ZONE_ID);
    }
}
