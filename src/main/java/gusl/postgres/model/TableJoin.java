package gusl.postgres.model;

import gusl.core.tostring.ToString;
import gusl.postgres.metadata.DOMetadata;
import gusl.query.MatchQuery;
import gusl.query.QueryParams;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 25/05/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class TableJoin {

    private DOMetadata metadata;
    private MatchQuery on;
    private QueryParams queryParams;

    public QueryParams createQueryParams() {
        queryParams = new QueryParams();
        return queryParams;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
