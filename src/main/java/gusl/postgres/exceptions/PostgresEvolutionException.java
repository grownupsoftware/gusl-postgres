package gusl.postgres.exceptions;

import gusl.core.exceptions.GUSLException;

/**
 * @author dhudson
 * @since 15/04/2021
 */
public class PostgresEvolutionException extends GUSLException {

    private static final long serialVersionUID = 1L;

    public PostgresEvolutionException(String message, Throwable ex) {
        super(message, ex);
    }

    public PostgresEvolutionException(String message) {
        super(message);
    }
}
