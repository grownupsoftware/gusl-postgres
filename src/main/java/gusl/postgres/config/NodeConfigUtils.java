package gusl.postgres.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.config.ConfigUtils;
import gusl.core.utils.IOUtils;
import gusl.core.utils.SystemPropertyUtils;
import lombok.CustomLog;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author dhudson
 */
@CustomLog
public class NodeConfigUtils {

    public static final String DEFAULT_CONFIG_FILE_NAME = "application.json";
    public static final ObjectMapper theMapper = ConfigUtils.createConfigMapper();

    private NodeConfigUtils() {
    }

    public static <T> T loadConfig(Class<T> config) throws IOException {
        if (SystemPropertyUtils.hasConfigLocation()) {
            logger.info("Loading config {}", SystemPropertyUtils.getGivenConfig());
            return ConfigUtils.loadConfig(config, SystemPropertyUtils.getGivenConfig(), theMapper);
        }

        // Lets load the default
        try (InputStream is = IOUtils.getResourceAsStream(DEFAULT_CONFIG_FILE_NAME, config.getClassLoader())) {
            logger.info("Loading default config {}", DEFAULT_CONFIG_FILE_NAME);
            return theMapper.readValue(is, config);
        } catch (IOException ex) {
            logger.error("Error reading config file: {}", DEFAULT_CONFIG_FILE_NAME);
            throw ex;
        }
    }

    public static <T> T loadConfig(Class<T> config, String configName) throws IOException {
        return ConfigUtils.loadConfig(config, configName, theMapper);
    }

}
