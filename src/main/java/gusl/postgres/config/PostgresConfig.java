package gusl.postgres.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.core.config.CoreCountDeserializer;
import gusl.core.config.ObfuscationDeserializer;
import gusl.core.config.SystemProperyDeserializer;
import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 15/04/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class PostgresConfig {

    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String database;
    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String connectionPoints;
    private Integer port;
    private String username;
    // @JsonDeserialize(using = ObfuscationDeserializer.class)
    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String password;
    private boolean ssl;
    private Integer maxConnections;
    private Integer prepareThreshold;
    @JsonDeserialize(using = CoreCountDeserializer.class)
    private Integer asyncThreads;
    private String replication;

    @JsonIgnore
    public String[] getServerNames() {
        return connectionPoints.split("\\|");
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
