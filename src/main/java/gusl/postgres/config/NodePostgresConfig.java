package gusl.postgres.config;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 19/04/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class NodePostgresConfig {
    private PostgresConfig postgresConfig;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
