package gusl.postgres.metadata;

import gusl.core.tostring.ToString;
import gusl.postgres.model.PostgresField;
import gusl.postgres.utils.PostgresClassUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Set;

/**
 * @author dhudson
 * @since 26/05/2021
 */
public class CustomMetadata<T> extends AbstractFieldListMetadata<T> implements Metadata<T> {

    public CustomMetadata(Class<T> doClass) {
        super(doClass);
        create();
    }

    public void create() {
        Set<Field> javaFields = PostgresClassUtils.getDataFieldsFor(getDOClass());
        fieldTupleList = new ArrayList<>(javaFields.size());

        for (Field field : javaFields) {
            PostgresField postgresField = field.getAnnotation(PostgresField.class);
            if (postgresField == null) {
                continue;
            }

            fieldTupleList.add(new FieldTuple(field, postgresField));
        }
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
