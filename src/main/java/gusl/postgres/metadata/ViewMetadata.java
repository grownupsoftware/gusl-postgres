package gusl.postgres.metadata;

import gusl.core.utils.ClassUtils;
import gusl.core.utils.StringUtils;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresView;
import gusl.query.QueryParams;
import gusl.query.RangeQuery;
import gusl.query.RangeQueryHelper;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Set;

/**
 * @author dhudson
 * @since 25/08/2022
 */
public class ViewMetadata<T> extends AbstractFieldListMetadata<T> implements Metadata<T> {

    private String theSQLStatement;
    private String theCountStatement;

    private boolean hasWhere;

    public ViewMetadata(Class<T> clazz) {
        super(clazz);
    }

    public void create(PostgresView annotation) {
        theSQLStatement = annotation.sql();
        theCountStatement = annotation.count();
        if (StringUtils.containsIgnoreCase(theSQLStatement, "where")) {
            hasWhere = true;
        }

        Set<Field> fields = ClassUtils.getFieldsFor(getDOClass(), true);
        fieldTupleList = new ArrayList<>(fields.size());
        for (Field field : fields) {
            PostgresField postgresField = field.getAnnotation(PostgresField.class);
            if (postgresField == null) {
                continue;
            }

            fieldTupleList.add(new FieldTuple(field, postgresField));
        }
    }

    public String getSelectStatement(QueryParams params) {
        return addQueryParams(theSQLStatement, params, true);
    }

    public String buildSelectCountStatement(QueryParams request) {
        return addQueryParams(theCountStatement, request, false);
    }

    public String addQueryParams(String sql, QueryParams params, boolean withPaging) {
        StringBuilder builder = new StringBuilder(sql);
        builder.append(processQueryParams(params, withPaging));
        builder.append(";");
        logger.debug("Sql statement: {}", builder.toString());
        return builder.toString();
    }

    @Override
    protected void processRangeQuery(StringBuilder builder, Set<RangeQuery> queries, boolean isFirst) {
        RangeQueryHelper.processRangeQuery(builder, queries, isFirst, hasWhere);
    }

    boolean getFirstPrefix(StringBuilder builder, boolean isFirst) {
        if (hasWhere) {
            builder.append(" AND ");
            return false;
        }

        if (isFirst) {
            builder.append(" WHERE ");
        } else {
            builder.append(" AND ");
        }
        return false;
    }

}
