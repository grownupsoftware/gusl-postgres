package gusl.postgres.metadata;

import gusl.annotations.form.page.OrderDirection;
import gusl.annotations.form.page.PageRequest;
import gusl.annotations.form.page.QueryCondition;
import gusl.annotations.form.page.QueryConditions;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.lambda.MutableBoolean;
import gusl.core.lambda.MutableInteger;
import gusl.core.security.ObfuscateMethod;
import gusl.core.utils.IdGenerator;
import gusl.core.utils.StringUtils;
import gusl.postgres.errors.PostgresErrors;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import gusl.postgres.model.PostgresTable;
import gusl.postgres.model.PostgresView;
import gusl.postgres.utils.PostgresClassUtils;
import gusl.postgres.utils.PostgresUtils;
import gusl.query.QueryParams;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.Predicate;

import static gusl.core.utils.LambdaGUSLErrorExceptionHelper.rethrowGUSLErrorConsumer;
import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * @author dhudson
 * @since 26/04/2021
 */
@Getter
@Setter
public class DOMetadata<T> extends AbstractFieldListMetadata<T> implements Metadata<T> {

    private final Class<?> mixinClass;
    private PostgresTable table;
    private PostgresView view;
    private Field javaPrimaryKey;
    private PostgresField postgresPrimaryKey;
    private Field createdField;
    private Field updatedField;
    private boolean createRequiresUpdate;
    private boolean populate;

    private List<FieldTuple> defaultFields = new ArrayList<>(0);
    private List<FieldTuple> securityFields = new ArrayList<>(0);

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private String theSelectStatement;

    public DOMetadata(Class<T> clazz, Class<?> mixin) {
        super(clazz);
        if (mixin == null) {
            mixinClass = clazz;
        } else {
            mixinClass = mixin;
        }
    }

    public void create(PostgresView viewAnnotation) {
        Set<Field> javaFields = PostgresClassUtils.getDataFieldsFor(getDOClass());
        // If DO and Mixin are the same, the cache will return the same fields
        Set<Field> javaMixinFields = PostgresClassUtils.getDataFieldsFor(mixinClass);

        Map<String, Field> doFieldMap = getJavaFieldMap(javaFields);
        Map<String, PostgresField> postgresFieldMap = new HashMap<>();
        Iterator<Field> iterator = javaMixinFields.iterator();
        while (iterator.hasNext()) {
            Field field = iterator.next();
            PostgresField postgresField = field.getAnnotation(PostgresField.class);
            if (postgresField == null) {
                // Transient field, do not map to
                iterator.remove();
                continue;
            }

            // Does the field belong in the DO class?
            if (!doFieldMap.containsKey(field.getName())) {
                iterator.remove();
                continue;
            }

            // We have an annotation and a field match
            postgresFieldMap.put(field.getName(), postgresField);
        }

        fieldTupleList = new ArrayList<>(postgresFieldMap.size());

        // Let's map the DO class with the MinIn class
        iterator = javaFields.iterator();
        while (iterator.hasNext()) {
            Field field = iterator.next();

            PostgresField postgresField = postgresFieldMap.get(field.getName());
            if (postgresField == null) {
                // Transient field, do not map to
                iterator.remove();
            } else {
                if (postgresField.primaryKey()) {
                    postgresPrimaryKey = postgresField;
                    javaPrimaryKey = field;
                }

                if (postgresField.dateCreated()) {
                    createdField = field;
                }
                if (postgresField.dateUpdated()) {
                    updatedField = field;
                    if (postgresField.notNull()) {
                        // If updated is not null, then need to be set on create
                        createRequiresUpdate = true;
                    }
                    // Always update dateUpdated
                    populate = postgresField.populate();
                }

                if (postgresField.securityMethod() != ObfuscateMethod.NONE) {
                    securityFields.add(new FieldTuple(field, postgresField));
                }

                if (StringUtils.isNotBlank(postgresField.defaultValue())) {
                    defaultFields.add(new FieldTuple(field, postgresField));
                }

                fieldTupleList.add(new FieldTuple(field, postgresField));
            }
        }
        view = viewAnnotation;
    }

    public void create(PostgresTable tableAnnotation) {
        Set<Field> javaFields = PostgresClassUtils.getDataFieldsFor(getDOClass());
        // If DO and Mixin are the same, the cache will return the same fields
        Set<Field> javaMixinFields = PostgresClassUtils.getDataFieldsFor(mixinClass);

        Map<String, Field> doFieldMap = getJavaFieldMap(javaFields);
        Map<String, PostgresField> postgresFieldMap = new HashMap<>();
        Iterator<Field> iterator = javaMixinFields.iterator();
        while (iterator.hasNext()) {
            Field field = iterator.next();
            PostgresField postgresField = field.getAnnotation(PostgresField.class);
            if (postgresField == null) {
                // Transient field, do not map to
                iterator.remove();
                continue;
            }

            // Does the field belong in the DO class?
            if (!doFieldMap.containsKey(field.getName())) {
                iterator.remove();
                continue;
            }

            // We have an annotation and a field match
            postgresFieldMap.put(field.getName(), postgresField);
        }

        fieldTupleList = new ArrayList<>(postgresFieldMap.size());

        // Let's map the DO class with the MinIn class
        iterator = javaFields.iterator();
        while (iterator.hasNext()) {
            Field field = iterator.next();

            PostgresField postgresField = postgresFieldMap.get(field.getName());
            if (postgresField == null) {
                // Transient field, do not map to
                iterator.remove();
            } else {
                if (postgresField.primaryKey()) {
                    postgresPrimaryKey = postgresField;
                    javaPrimaryKey = field;
                }

                if (postgresField.dateCreated()) {
                    createdField = field;
                }
                if (postgresField.dateUpdated()) {
                    updatedField = field;
                    if (postgresField.notNull()) {
                        // If updated is not null, then need to be set on create
                        createRequiresUpdate = true;
                    }
                    // Always update dateUpdated
                    populate = postgresField.populate();
                }

                if (postgresField.securityMethod() != ObfuscateMethod.NONE) {
                    securityFields.add(new FieldTuple(field, postgresField));
                }

                if (StringUtils.isNotBlank(postgresField.defaultValue())) {
                    defaultFields.add(new FieldTuple(field, postgresField));
                }

                fieldTupleList.add(new FieldTuple(field, postgresField));
            }
        }

        table = tableAnnotation;
    }

    public String getTableName() {
        return nonNull(table) ? table.table() : view.name();
    }

    public <VIEW> DOMetadata<VIEW> createView(Class<VIEW> view) {
        DOMetadata<VIEW> metadata = new DOMetadata<>(view, null);
        Set<Field> viewFields = PostgresClassUtils.getDataFieldsFor(view);
        metadata.fieldSizeHint(viewFields.size());

        // Find matching fields
        Iterator<Field> fields = viewFields.iterator();
        while (fields.hasNext()) {
            Field field = fields.next();
            if (nonNull(field)) {
                FieldTuple tuple = getFieldTupleFor(field.getName());
                if (tuple != null) {
                    PostgresField postgresField = tuple.getPostgresField();
                    metadata.addFieldTuple(new FieldTuple(field, postgresField));
                    if (postgresField.primaryKey()) {
                        metadata.setJavaPrimaryKey(field);
                        metadata.setPostgresPrimaryKey(postgresField);
                    }
                }
            }
        }

        metadata.setTable(getTable());
        return metadata;
    }

    private Map<String, Field> getJavaFieldMap(Set<Field> javaFields) {
        Map<String, Field> doFieldMap = new HashMap<>();
        javaFields.forEach(field -> doFieldMap.put(field.getName(), field));
        return doFieldMap;
    }

    public String getSelectStatement() {
        if (theSelectStatement == null) {
            theSelectStatement = buildSelectStatement();
        }
        return theSelectStatement;
    }

    public String addQueryParams(String sql, QueryParams params) {
        return addQueryParams(sql, params, true);
    }

    public String addQueryParams(String sql, QueryParams params, boolean withPaging) {
        StringBuilder builder = new StringBuilder(sql);
        builder.append(processQueryParams(params, withPaging));
        builder.append(";");
        logger.debug("Sql statement: {}", builder.toString());
        return builder.toString();
    }

    public String getSelectStatement(QueryParams params) {
        return addQueryParams(getSelectStatement(), params);
    }

    public String getSelectFieldsWithPrefix(String prefix) {
        StringBuilder builder = new StringBuilder(128);

        for (int i = 0; i < getFieldCount(); i++) {
            if (i > 0) {
                builder.append(",");
            }

            builder.append(prefix).append(".").append(getJavaField(i).getName());
        }

        return builder.toString();
    }

    private String buildSelectStatement() {
        StringBuilder builder = new StringBuilder(128);
        builder.append("SELECT ");

        for (int i = 0; i < getFieldCount(); i++) {
            if (i > 0) {
                builder.append(",");
            }

            builder.append(getJavaField(i).getName());
        }

        builder.append(" FROM ");
        builder.append(getTableName());

        return builder.toString();
    }

    public String buildSelectCountStatement(QueryParams request) {
        return addQueryParams("SELECT count(*)  AS rowcount FROM " + getTableName(), request, false);
    }

    private void bindValueFromComplexData(String fromName, PreparedStatement statement, Field javaField, PostgresField postgresField, int sqlIndex, Map<String, Object> complexValue) throws GUSLErrorException {
        if (nonNull(complexValue) && nonNull(fromName)) {
            PostgresUtils.bindStatementValue(statement, javaField, postgresField, sqlIndex, complexValue.get(fromName));
        } else {
            PostgresUtils.bindStatementValue(statement, javaField, postgresField, sqlIndex, "");
        }
    }

    public void updateInsert(T entity) throws GUSLErrorException {
        try {
            if (createdField != null) {
                // Only set if null
                if (createdField.get(entity) == null) {
                    updateWithNow(createdField, entity);
                }
            }

            updateUpdate(entity);

            if (postgresPrimaryKey.populate()) {
                Object value = javaPrimaryKey.get(entity);
                if (value == null) {
                    if (postgresPrimaryKey.type() == PostgresFieldType.BIGINT) {
                        javaPrimaryKey.set(entity, IdGenerator.generateUniqueNodeId());
                    } else {
                        javaPrimaryKey.set(entity, IdGenerator.generateUniqueNodeIdAsString());
                    }
                }
            }

            updateDefaultFields(entity);

        } catch (IllegalAccessException ex) {
            throw PostgresErrors.AUTO_FILL_ERROR.generateException(ex);
        }
    }

    private void updateDefaultFields(T entity) throws GUSLErrorException {
        for (FieldTuple tuple : defaultFields) {
            try {
                Field javaField = tuple.getJavaField();
                PostgresField postgresField = tuple.getPostgresField();
                if (javaField.get(entity) == null) {
                    javaField.set(entity, PostgresUtils.getObjectValue(postgresField, postgresField.defaultValue()));
                }
            } catch (IllegalAccessException ex) {
                throw PostgresErrors.DEFAULT_ERROR.generateException(ex, tuple.getJavaField().getName(), tuple.getPostgresField().defaultValue());
            }
        }
    }

    public void updateUpdate(T entity) throws GUSLErrorException {
        if (updatedField != null) {
            try {
                if (populate || updatedField.get(entity) == null) {
                    updateWithNow(updatedField, entity);
                }
            } catch (IllegalAccessException ex) {
                throw PostgresErrors.AUTO_FILL_ERROR.generateException(ex);
            }
        }
    }

    private void updateWithNow(Field field, T entity) throws IllegalAccessException {
        Class<?> type = field.getType();
        if (type == LocalDateTime.class) {
            field.set(entity, PostgresUtils.now());
        }
        if (type == ZonedDateTime.class) {
            field.set(entity, ZonedDateTime.now());
        }
    }

    // -------------------------------- here until supernova migrated

    public String buildSelectCountStatement(PageRequest request) {
        StringBuilder builder = new StringBuilder(64);
        builder.append("SELECT count(*)  AS rowcount FROM ").append(getTableName());
        addQueryConditions(builder, request.getQueryConditions());
        builder.append(";");
        return builder.toString();
    }

    public void bindQueryStatement(PreparedStatement statement, PageRequest request) throws GUSLErrorException {
        if (nonNull(request)) {
            if (nonNull(request.getQueryConditions())) {
                MutableInteger sqlIndex = new MutableInteger(1);
                safeStream(request.getQueryConditions().getConditions()).filter(Objects::nonNull).filter(validateCondition()).forEach(rethrowGUSLErrorConsumer(condition -> {

                    if (isNull(condition.getOperand())) {
                        bind(statement, condition, sqlIndex);
                    } else {
                        switch (condition.getOperand()) {
                            case EQUALS:
                            case QUERY_STRING:
                            case NOT_EQUALS:
                                bind(statement, condition, sqlIndex);
                                break;
                            case DATE_RANGE:
                                bindDateRange(condition, statement, sqlIndex);
                                break;
                        }
                    }
                }));
            }

        }
    }

    private void addQueryConditions(StringBuilder builder, QueryConditions queryConditions) {
        if (nonNull(queryConditions)) {
            MutableBoolean isFirst = new MutableBoolean(true);
            safeStream(queryConditions.getConditions()).filter(validateCondition()).forEach(condition -> {
                getFirstPrefix(builder, isFirst.get());
                isFirst.set(false);
                builder.append(condition.getFieldName());
                if (isNull(condition.getOperand())) {
                    builder.append("=?");
                } else {
                    switch (condition.getOperand()) {
                        default:
                        case EQUALS:
                            builder.append("=?");
                            break;
                        case QUERY_STRING:
                            builder.append("~~*?");
                            break;
                        case DATE_RANGE:
                            builder.append(addDateRange(condition));
                            break;
                        case NOT_EQUALS:
                            builder.append("!=?");
                            break;
                    }
                }
            });
        }
    }

    private String addDateRange(QueryCondition condition) {
        if (isNull(condition) || isNull(condition.getDateRange())) {
            return "";
        }

        if (nonNull(condition.getDateRange().getFrom()) && nonNull(condition.getDateRange().getTo())) {
            return " >= ?::timestamp AND " + condition.getFieldName() + " <= ?::timestamp ";
        } else if (nonNull(condition.getDateRange().getFrom())) {
            return " >= ?::timestamp ";
        } else if (nonNull(condition.getDateRange().getTo())) {
            return " <= ?::timestamp ";
        } else {
            return "";
        }
    }

    private Predicate<QueryCondition> validateCondition() {
        return queryCondition -> {
            if (isNull(queryCondition)
                    // || isNull(queryCondition.getOperand())
                    || isNull(queryCondition.getFieldName()) || (isNull(queryCondition.getValue()) && isNull(queryCondition.getDateRange()) && isNull(queryCondition.getComplexValue()))) {
                return false;
            }
            return true;
        };
    }

    private void bind(PreparedStatement statement, QueryCondition condition, MutableInteger sqlIndex) throws GUSLErrorException {
        FieldTuple tuple = findFieldByName(condition.getFieldName());
        if (nonNull(condition.getValue())) {
            PostgresUtils.bindStatementValue(statement, tuple.getJavaField(), tuple.getPostgresField(), sqlIndex.getAndIncrement(), condition.getValue());
        } else {
            bindValueFromComplexData(condition.getFromName(), statement, tuple.getJavaField(), tuple.getPostgresField(), sqlIndex.getAndIncrement(), condition.getComplexValue());
        }
    }

    private void bindDateRange(QueryCondition condition, PreparedStatement statement, MutableInteger sqlIndex) throws GUSLErrorException {
        try {
            if (nonNull(condition.getDateRange().getFrom()) && nonNull(condition.getDateRange().getTo())) {
                statement.setObject(sqlIndex.getAndIncrement(), addStartOfDay(condition.getDateRange().getFrom()));
                statement.setObject(sqlIndex.getAndIncrement(), addEndOfDay(condition.getDateRange().getTo()));
            } else if (nonNull(condition.getDateRange().getFrom())) {
                statement.setObject(sqlIndex.getAndIncrement(), addStartOfDay(condition.getDateRange().getFrom()));
            } else if (nonNull(condition.getDateRange().getTo())) {
                statement.setObject(sqlIndex.getAndIncrement(), addStartOfDay(condition.getDateRange().getTo()));
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    public String getSelectStatement(PageRequest request) {
        StringBuilder builder = new StringBuilder(getSelectStatement());

        if (nonNull(request)) {

            addQueryConditions(builder, request.getQueryConditions());

            if (nonNull(request.getOrderParam())) {
                builder.append(" ORDER BY ");
                builder.append(request.getOrderParam());
                builder.append(" ");
                if (isNull(request.getOrderDirection()) || request.getOrderDirection() == OrderDirection.ASC) {
                    builder.append("ASC");
                } else {
                    builder.append("DESC");
                }
            }

            if (request.getPerPage() > 0) {
                builder.append(" LIMIT ");
                builder.append(request.getPerPage());
            }

            if (request.getOffset() > 0) {
                builder.append(" OFFSET ");
                builder.append(request.getOffset() * request.getPerPage());
            }
        }
        builder.append(";");
        // logger.info("sql: {}", builder.toString());
        return builder.toString();
    }

    public List<String> getFieldNames() {
        List<String> headers = new ArrayList<>(getFieldTupleList().size());
        for (FieldTuple tuple : getFieldTupleList()) {
            headers.add(tuple.getJavaField().getName());
        }
        return headers;
    }
}
