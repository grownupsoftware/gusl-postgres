package gusl.postgres.metadata;

/**
 * @author dhudson
 * @since 26/05/2021
 */
public interface Metadata<T> {

    FieldTuple getFieldTupleAt(int index);

    FieldTuple getFieldTupleFor(String fieldName);

    Class<T> getDOClass();

    int getFieldCount();
}
