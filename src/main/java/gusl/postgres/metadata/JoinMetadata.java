package gusl.postgres.metadata;

import gusl.core.exceptions.GUSLErrorException;
import gusl.postgres.model.TableJoin;
import lombok.CustomLog;

import java.sql.PreparedStatement;

/**
 * @author dhudson
 * @since 25/05/2021
 */
@CustomLog
public class JoinMetadata<T> extends AbstractMetadata<T> implements Metadata<T> {

    private final DOMetadata table1;
    private final TableJoin join;

    public JoinMetadata(DOMetadata<T> table1, TableJoin join) {
        super(table1.getDOClass());
        this.table1 = table1;
        this.join = join;
    }

    private String getSelectStatement() {
        StringBuilder builder = new StringBuilder(128);
        builder.append("SELECT ");

        for (int i = 0; i < table1.getFieldCount(); i++) {
            if (i > 0) {
                builder.append(",");
            }

            builder.append(table1.getTableName());
            builder.append(".");
            builder.append(table1.getJavaField(i).getName());
        }

        builder.append(" FROM ");
        builder.append(table1.getTableName());

        builder.append(" JOIN ");
        builder.append(join.getMetadata().getTableName());
        builder.append(" ON ");
        builder.append(join.getOn().getField());
        builder.append(" = ");
        builder.append(join.getOn().getValue());
        builder.append(" ");


        return builder.toString();
    }

    public String getStatement() {
        StringBuilder builder = new StringBuilder(getSelectStatement());
        builder.append(processQueryParams(join.getQueryParams()));
        builder.append(";");

        logger.debug("Sql statement: {}", builder.toString());
        return builder.toString();
    }

    public void bindQueryStatement(PreparedStatement statement) throws GUSLErrorException {
        bindQueryStatement(statement, join.getQueryParams());
    }

    @Override
    public FieldTuple getFieldTupleAt(int index) {
        return table1.getFieldTupleAt(index);
    }

    @Override
    public FieldTuple getFieldTupleFor(String name) {
        int index = name.indexOf(TABLE_FIELD);
        if (index > 0) {
            String tableName = name.substring(0, index);
            String fieldName = name.substring(index + 1);
            if (tableName.equalsIgnoreCase(table1.getTableName())) {
                return table1.getFieldTupleFor(fieldName);
            }

            // Lets assume its the other one
            return join.getMetadata().getFieldTupleFor(fieldName);
        }

        // No table prefix, lets assume the first table
        return table1.getFieldTupleFor(name);
    }

    @Override
    public int getFieldCount() {
        return table1.getFieldCount();
    }
}
