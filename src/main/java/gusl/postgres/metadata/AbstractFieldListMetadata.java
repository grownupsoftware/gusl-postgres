package gusl.postgres.metadata;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.tostring.ToString;
import gusl.postgres.errors.PostgresErrors;
import gusl.postgres.model.PostgresField;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dhudson
 * @since 26/05/2021
 */
public abstract class AbstractFieldListMetadata<T> extends AbstractMetadata<T> {

    protected List<FieldTuple> fieldTupleList;

    public AbstractFieldListMetadata(Class<T> clazz) {
        super(clazz);
    }

    @Override
    public FieldTuple getFieldTupleAt(int index) {
        return fieldTupleList.get(index);
    }

    @Override
    public FieldTuple getFieldTupleFor(String fieldName) {
        for (int i = 0; i < fieldTupleList.size(); i++) {
            FieldTuple tuple = fieldTupleList.get(i);
            if (tuple.getJavaField().getName().equals(fieldName)) {
                return tuple;
            }
        }
        return null;
    }

    public FieldTuple findFieldByName(String fieldName) throws GUSLErrorException {
        FieldTuple tuple = getFieldTupleFor(fieldName);
        if (tuple == null) {
            logger.error("Failed to find {} on {}", fieldName, getDOClass().getName());
            throw PostgresErrors.QUERY_ERROR.generateException(fieldName, getDOClass().getName());
        }
        return tuple;
    }

    @Override
    public int getFieldCount() {
        return fieldTupleList.size();
    }

    public Field getJavaField(int index) {
        FieldTuple tuple = getFieldTupleAt(index);
        if (tuple == null) {
            return null;
        }
        return tuple.getJavaField();
    }

    public PostgresField getPostgresField(int index) {
        FieldTuple tuple = getFieldTupleAt(index);
        if (tuple == null) {
            return null;
        }
        return tuple.getPostgresField();
    }

    public void addFieldTuple(FieldTuple tuple) {
        if (fieldTupleList == null) {
            fieldTupleList = new ArrayList<>();
        }
        fieldTupleList.add(tuple);
    }

    public void fieldSizeHint(int hint) {
        if (fieldTupleList == null) {
            fieldTupleList = new ArrayList<>(hint);
        }
    }

    public List<FieldTuple> getFieldTupleList() {
        return fieldTupleList;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
