package gusl.postgres.metadata;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.lambda.MutableInteger;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.security.ObfuscateMethod;
import gusl.core.security.ObfuscatedStorage;
import gusl.core.time.GUSLZonedDateTime;
import gusl.core.time.TimeService;
import gusl.core.utils.ClassUtils;
import gusl.core.utils.DateFormats;
import gusl.core.utils.Utils;
import gusl.postgres.errors.PostgresErrors;
import gusl.postgres.model.PostgresFieldType;
import gusl.postgres.utils.PostgresUtils;
import gusl.query.*;
import org.postgresql.util.PGobject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Comparator.*;
import static java.util.Objects.nonNull;

/**
 * @author dhudson
 * @since 25/05/2021
 */
public abstract class AbstractMetadata<T> {

    private final Class<T> DOClass;

    protected GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    private static final DateTimeFormatter POSTGRES_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    static final String JSON_FIELD = "->>";
    static final String TABLE_FIELD = ".";

    public AbstractMetadata(Class<T> clazz) {
        DOClass = clazz;
    }

    abstract FieldTuple getFieldTupleAt(int index);

    abstract FieldTuple getFieldTupleFor(String fieldName);

    abstract int getFieldCount();

    public Class<T> getDOClass() {
        return DOClass;
    }

    protected void processRangeQuery(StringBuilder builder, Set<RangeQuery> queries, boolean isFirst) {
        RangeQueryHelper.processRangeQuery(builder, queries, isFirst);
    }

    boolean getFirstPrefix(StringBuilder builder, boolean isFirst) {
        if (isFirst) {
            builder.append(" WHERE ");
        } else {
            builder.append(" AND ");
        }
        return false;
    }

    boolean getFirstOrPrefix(StringBuilder builder, boolean isFirst) {
        if (isFirst) {
            builder.append(" WHERE ");
        } else {
            builder.append(" OR ");
        }
        return false;
    }

    // https://www.postgresql.org/docs/13/sql-select.html
    String processQueryParams(QueryParams params) {
        return processQueryParams(params, true);
    }

    private String addMatchQuery(MatchQuery matchQuery) {
        if (matchQuery.isFuzzy()) {
            return matchQuery.getField() + " ~* ? ";
        }
        if (isLike(matchQuery.getValue())) {
            return matchQuery.getField() + " LIKE ? ";
        }

        if (matchQuery.isCaseInsensitive()) {
            return " lower(" + matchQuery.getField() + ") = ? ";
        }

        return matchQuery.getField() + " = ? ";
    }

    private String addNotMatchQuery(MatchQuery matchQuery) {
        if (matchQuery.isFuzzy()) {
            return matchQuery.getField() + " !~* ? ";
        }
        if (isLike(matchQuery.getValue())) {
            return matchQuery.getField() + " NOT LIKE ? ";
        }

        if (matchQuery.isCaseInsensitive()) {
            return " lower(" + matchQuery.getField() + ") != ? ";
        }

        return matchQuery.getField() + " != ? ";
    }

    String processQueryParams(QueryParams params, boolean withPaging) {
        StringBuilder builder = new StringBuilder(128);

        if (params != null) {
            boolean isFirst = true;

            modifyMatchQueryIfNotText(params.getAllMatchQueries());

            for (MatchQuery matchQuery : Utils.safeCollection(params.getMusts())) {
                isFirst = getFirstPrefix(builder, isFirst);
                builder.append(addMatchQuery(matchQuery));
            }

            for (MatchQuery matchQuery : Utils.safeCollection(params.getMustNots())) {
                isFirst = getFirstPrefix(builder, isFirst);
                builder.append(addNotMatchQuery(matchQuery));
            }

            isFirst = processShould(builder, params.getShould(), isFirst);

            for (InQuery inQuery : Utils.safeCollection(params.getIns())) {
                if (inQuery.getValues() != null && inQuery.getValues().length > 0) {
                    isFirst = getFirstPrefix(builder, isFirst);
                    builder.append(inQuery.getField());
                    builder.append(" in (");
                    boolean doneFirst = false;
                    for (Object val : inQuery.getValues()) {
                        if (doneFirst) {
                            builder.append(",?");
                        } else {
                            doneFirst = true;
                            builder.append("?");
                        }
                    }
                    builder.append(" ) ");
                }
            }

            // Range Queries
            processRangeQuery(builder, params.getRangeQueries(), isFirst);

            if (withPaging) {
                if (nonNull(params.getOrderBys()) && !params.getOrderBys().isEmpty()) {
                    builder.append(" ORDER BY ");
                    MutableInteger count = new MutableInteger(0);
                    safeStream(params.getOrderBys())
                            .sorted(nullsLast(comparing(OrderBy::getPositionOrder, nullsLast(naturalOrder()))))
                            .forEach(orderBy -> {
                                if (count.incrementAndGet() > 1) {
                                    builder.append(" , ");
                                }
                                builder.append(orderBy.getField());
                                builder.append(" ");
                                builder.append(orderBy.getOrder());
                            });
                }

                if (params.getLimit() > 0) {
                    builder.append(" LIMIT ");
                    builder.append(params.getLimit());
                }
                if (params.getSkip() != 0) {
                    builder.append(" OFFSET ");
                    builder.append(params.getSkip());
                }
            }
        }

        return builder.toString();
    }

    private boolean processShould(StringBuilder builder, Set<MatchQuery> should, boolean isFirst) {
        if (!Utils.hasElements(should)) {
            return isFirst;
        }

        boolean first = isFirst;
        List<MatchQuery> shoulds = new ArrayList<>(should);
        if (shoulds.size() == 1) {
            first = getFirstPrefix(builder, first);
            builder.append(addMatchQuery(shoulds.get(0)));
            return first;
        }

        first = getFirstPrefix(builder, first);

        builder.append(" ( ");
        Iterator<MatchQuery> iterator = shoulds.iterator();
        while (iterator.hasNext()) {
            builder.append(addMatchQuery(iterator.next()));
            if (iterator.hasNext()) {
                first = getFirstOrPrefix(builder, first);
            }
        }
        builder.append(" ) ");

        return first;
    }

    private void modifyMatchQueryIfNotText(List<MatchQuery> matchQueries) {
        for (MatchQuery query : matchQueries) {
            FieldTuple tuple = getFieldTupleFor(query.getField());
            if (tuple != null) {
                if (tuple.getPostgresField().type() != PostgresFieldType.TEXT) {
                    query.setFuzzy(false);
                    if (query.getValue() instanceof String) {
                        String value = (String) query.getValue();
                        // needs to convert
                        Class<?> type = tuple.getJavaField().getType();
                        if (type == int.class || type == Integer.class) {
                            query.setValue(Integer.valueOf(value));
                        }
                        if (type == double.class || type == Double.class) {
                            query.setValue(Double.valueOf(value));
                        }
                        if (type == long.class || type == Long.class) {
                            query.setValue(Long.valueOf(value));
                        }
                    }
                }
            }
        }
    }

    private boolean isLike(Object value) {
        if (value == null) {
            return false;
        }

        return value.toString().contains("%");
    }

    LocalDateTime convertToLocalDateTime(String dateStr) {
        try {
            final Date date = DateFormats.getFormatFor("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse(dateStr);
            return date.toInstant().atZone(PostgresUtils.ZONE_ID).toLocalDateTime();
        } catch (ParseException e) {
            logger.error("Failed to parse date: {}", dateStr, e);
            return null;
        }
    }

    String addStartOfDay(Date date) {
        return POSTGRES_FORMAT.format(date.toInstant().atZone(PostgresUtils.ZONE_ID).toLocalDateTime());
    }

    String addEndOfDay(Date date) {
        return POSTGRES_FORMAT.format(date.toInstant().atZone(PostgresUtils.ZONE_ID).toLocalDateTime());
    }

    public void bindQueryStatement(PreparedStatement statement, QueryParams params) throws GUSLErrorException {
        if (params != null) {
            int sqlIndex = 1;

            for (MatchQuery matchQuery : params.getAllMatchQueries()) {
                if (matchQuery.getField().contains(JSON_FIELD)) {
                    try {
                        statement.setObject(sqlIndex++, matchQuery.getValue());
                    } catch (SQLException ex) {
                        throw PostgresErrors.SQL_ERROR.generateException(ex);
                    }
                } else {
                    if (matchQuery.getField().contains("::") || matchQuery.getField().contains("cast(")) {
                        try {
                            statement.setObject(sqlIndex++, matchQuery.getValue());
                        } catch (SQLException ex) {
                            logger.error("Failed to bind [::] [{}] to {} - error {}", matchQuery.getField(), matchQuery.getValue(), ex.getLocalizedMessage());
                            throw PostgresErrors.BIND_ERROR.generateException(ex, ex.getLocalizedMessage());
                        }
                    } else {
                        FieldTuple tuple = getFieldTupleFor(matchQuery.getField());
                        if (tuple == null) {
                            throw PostgresErrors.BIND_ERROR.generateException("Unknown field in match query " + matchQuery.getField());
                        }
                        PostgresUtils.bindStatementValue(statement, tuple.getJavaField(), tuple.getPostgresField(), sqlIndex++, matchQuery.getValue(), matchQuery.isCaseInsensitive());
                    }
                }
            }

            if (params.getIns() != null) {
                for (InQuery inQuery : params.getIns()) {
                    if (inQuery.getValues() != null && inQuery.getValues().length > 0) {
                        FieldTuple tuple = getFieldTupleFor(inQuery.getField());
                        for (Object val : inQuery.getValues()) {
                            PostgresUtils.bindStatementValue(statement, tuple.getJavaField(), tuple.getPostgresField(), sqlIndex++, val);
                        }
                    }
                }
            }

            for (RangeQuery rangeQuery : Utils.safeCollection(params.getRangeQueries())) {
                FieldTuple tuple = getFieldTupleFor(rangeQuery.getField());
                if (rangeQuery.isBetween()) {
                    PostgresUtils.bindStatementValue(statement, tuple.getJavaField(), tuple.getPostgresField(), sqlIndex++, rangeQuery.getFrom());
                    PostgresUtils.bindStatementValue(statement, tuple.getJavaField(), tuple.getPostgresField(), sqlIndex++, rangeQuery.getTo());
                } else {
                    PostgresUtils.bindStatementValue(statement, tuple.getJavaField(), tuple.getPostgresField(), sqlIndex++, rangeQuery.getSingleBindValue());
                }
            }

        }

    }

    public T parse(ResultSet rs) throws GUSLErrorException {
        T thing = null;
        try {
            thing = ClassUtils.getNewInstance(getDOClass());
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException ex) {
            logger.error("Failed to create instance of: {}", getDOClass());
            throw PostgresErrors.BIND_ERROR.generateException(ex, ex.getLocalizedMessage());
        }
        for (int i = 0; i < getFieldCount(); i++) {
            FieldTuple tuple = getFieldTupleAt(i);
            Field field = tuple.getJavaField();

            Object value = null;
            try {
                value = rs.getObject(tuple.getName());
            } catch (Throwable ex) {
                // Distinct doesn't have all the columns,
                // logger.error("Failed to get object for: {} {} {}", field.getName(), tuple.getName(), rs, ex);
            }
            if (value == null) {
                try {
                    field.set(thing, value);
                } catch (IllegalAccessException ex) {
                    logger.error("Failed to bind NULL to {} {} {} - error {}", thing.getClass().getName(), field.getName(), field.getType(), ex.getLocalizedMessage());
                    throw PostgresErrors.BIND_ERROR.generateException(ex, ex.getLocalizedMessage());
                }
                continue;
            }

            if (field.getType().isEnum()) {
                try {
                    field.set(thing, Enum.valueOf((Class<? extends Enum>) field.getType(), (String) value));
                } catch (IllegalAccessException ex) {
                    logger.error("Failed to bind Enum {} to {} {} {} - error {}", value, thing.getClass().getName(), field.getName(), field.getType(), ex.getLocalizedMessage());
                    throw PostgresErrors.BIND_ERROR.generateException(ex, ex.getLocalizedMessage());
                }
                continue;
            }

            switch (tuple.getPostgresField().type()) {
                case CHAR_ARRAY:
                    PGobject string = (PGobject) value;
                    try {
                        field.set(thing, string.getValue());
                    } catch (IllegalAccessException ex) {
                        logger.error("Failed to bind JSONB {} to {} {} {} - error {}", string, thing.getClass().getName(), field.getName(), field.getType(), ex.getLocalizedMessage());
                    }
                    break;
                case ZONED_TIMESTAMP:
                    PGobject time = (PGobject) value;
                    try {
                        GUSLZonedDateTime wrapper = PostgresUtils.OBJECT_MAPPER.readValue(time.getValue(), GUSLZonedDateTime.class);
                        field.set(thing, TimeService.convertESTDST(wrapper.getZonedDateTime()));
                    } catch (IOException ex) {
                        logger.error("Can't parse Json {} to {}", value, field.getName(), ex);
                    } catch (IllegalAccessException ex) {
                        logger.error("Failed to bind ZONED_TIMESTAMP {} to {} {} {} - error {}", time, thing.getClass().getName(), field.getName(), field.getType(), ex.getLocalizedMessage());
                    }
                    break;
                case JSONB:
                    PGobject json = (PGobject) value;
                    try {
                        field.set(thing, PostgresUtils.OBJECT_MAPPER.readValue(json.getValue(), field.getType()));
                    } catch (IOException ex) {
                        logger.error("Can't parse Json {} to {}", value, field.getName(), ex);
                    } catch (IllegalAccessException ex) {
                        logger.error("Failed to bind JSONB {} to {} {} {} - error {}", json, thing.getClass().getName(), field.getName(), field.getType(), ex.getLocalizedMessage());
                    }
                    break;
                case LIST:
                    PGobject jsonList = (PGobject) value;
                    try {
                        field.set(thing, PostgresUtils.OBJECT_MAPPER.readValue(jsonList.getValue(), tuple.getListType()));
                    } catch (IOException ex) {
                        logger.error("Can't parse Json {} to {}", value, field.getName(), ex);
                    } catch (IllegalAccessException ex) {
                        logger.error("Failed to bind JSONB {} to {} {} {} - error {}", jsonList, thing.getClass().getName(), field.getName(), field.getType(), ex.getLocalizedMessage());
                    }

                    break;
                case TIMESTAMP:
                    final LocalDateTime val;
                    try {
                        val = rs.getObject(tuple.getName(), LocalDateTime.class);
                    } catch (SQLException ex) {
                        logger.error("Failed to read TIMESTAMP field #{} to {} {} {} - error {}", i, thing.getClass().getName(), field.getName(), field.getType(), ex.getLocalizedMessage());
                        throw PostgresErrors.BIND_ERROR.generateException(ex, ex.getLocalizedMessage());
                    }
                    try {
                        if (field.getType() == LocalDateTime.class) {
                            field.set(thing, val);
                        }
                        if (field.getType() == ZonedDateTime.class) {
                            field.set(thing, val.atZone(PostgresUtils.ZONE_ID));
                        }
                    } catch (IllegalAccessException ex) {
                        logger.error("Failed to bind TIMESTAMP {} to {} {} {} - error {}", val, thing.getClass().getName(), field.getName(), field.getType(), ex.getLocalizedMessage());
                        throw PostgresErrors.BIND_ERROR.generateException(ex, ex.getLocalizedMessage());
                    }
                    break;
                case DATE:
                    try {
                        field.set(thing, rs.getObject(tuple.getName(), LocalDate.class));
                    } catch (IllegalAccessException | SQLException ex) {
                        logger.error("Failed to bind DATE field #{} to {} {} {} - error {}", i, thing.getClass().getName(), field.getName(), field.getType(), ex.getLocalizedMessage());
                        throw PostgresErrors.BIND_ERROR.generateException(ex, ex.getLocalizedMessage());
                    }
                    break;
                case TIME:
                    try {
                        field.set(thing, rs.getObject(tuple.getName(), LocalTime.class));
                    } catch (IllegalAccessException | SQLException ex) {
                        logger.error("Failed to bind TIME field #{} to {} {} {} - error {}", i, thing.getClass().getName(), field.getName(), field.getType(), ex.getLocalizedMessage());
                        throw PostgresErrors.BIND_ERROR.generateException(ex, ex.getLocalizedMessage());
                    }
                    break;
                case TEXT:
                    try {
                        String str = (String) value;
                        if (tuple.getPostgresField().securityMethod() == ObfuscateMethod.OBFUSCATED_WRITE_READ) {
                            if (ObfuscatedStorage.isObfuscated(str)) {
                                str = ObfuscatedStorage.decryptKey(str);
                            }
                        }
                        field.set(thing, str);
                    } catch (Throwable ex) {
                        logger.error("Failed to bind TEXT field #{} to {} {} {} - error {}", i,
                                thing.getClass().getName(), field.getName(), field.getType(), ex.getLocalizedMessage());
                        throw PostgresErrors.BIND_ERROR.generateException(ex, ex.getLocalizedMessage());
                    }
                    break;
                case NUMERIC:
                    try {
                        if (field.getType() == Double.class) {
                            field.set(thing, ((BigDecimal) value).doubleValue());
                        } else if (field.getType() == BigDecimal.class) {
                            field.set(thing, value);
                        }
                    } catch (IllegalAccessException ex) {
                        logger.error("Failed to bind NUMERIC value {} to {} {} {} - error {}", value, thing.getClass().getName(), field.getName(), field.getType(), ex.getLocalizedMessage());
                        throw PostgresErrors.BIND_ERROR.generateException(ex, ex.getLocalizedMessage());
                    }
                    break;
                default:
                    // Let's try the default
                    try {
                        field.set(thing, value);
                    } catch (IllegalAccessException ex) {
                        logger.error("Failed to bind DEFAULT value {} to {} {} {} - error {}", value, thing.getClass().getName(), field.getName(), field.getType(), ex.getLocalizedMessage());
                        throw PostgresErrors.BIND_ERROR.generateException(ex, ex.getLocalizedMessage());
                    }
            }
        }

        return thing;
    }

    public <T> T parseSingle(ResultSet rs) throws GUSLErrorException {
        try {
            if (!rs.isBeforeFirst()) {
                return null;
            }

            rs.next();

            return (T) parse(rs);

        } catch (SQLException ex) {
            throw PostgresErrors.PARSE_LIST_ERROR.generateException(ex, ex.getLocalizedMessage());
        }
    }

    public <T> List<T> parseList(ResultSet rs, boolean isFirstLast) throws GUSLErrorException {
        try {
            if (!rs.isBeforeFirst()) {
                return new ArrayList<>(0); // Collections.emptyList() return unmodifiable list;
            }

            List<T> list;
            if (isFirstLast) {
                list = new ArrayList<>(2);
                rs.next();
                list.add((T) parse(rs));
                rs.last();
                list.add((T) parse(rs));
            } else {
                list = new ArrayList<>();

                while (rs.next()) {
                    list.add((T) parse(rs));
                }
            }
            return list;
        } catch (SQLException ex) {
            throw PostgresErrors.PARSE_LIST_ERROR.generateException(ex, ex.getLocalizedMessage());
        }
    }
}
