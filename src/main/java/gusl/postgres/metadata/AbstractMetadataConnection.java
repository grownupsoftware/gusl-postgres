package gusl.postgres.metadata;

import gusl.core.exceptions.GUSLErrorException;
import gusl.postgres.application.DatabaseColumn;
import gusl.postgres.application.DatabaseIndex;
import gusl.postgres.application.DatabaseTable;
import gusl.postgres.errors.PostgresErrors;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dhudson
 * @since 10/08/2021
 */
public abstract class AbstractMetadataConnection {

    public AbstractMetadataConnection() {
    }

    // https://docs.oracle.com/javase/8/docs/api/java/sql/DatabaseMetaData.html
    public DatabaseMetaData getDatabaseMetaData(Connection connection) throws GUSLErrorException {
        try {
            return connection.getMetaData();
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    public List<DatabaseTable> getDatabaseViews(Connection connection) throws GUSLErrorException {
        try {
            List<DatabaseTable> tables = new ArrayList<>();

            DatabaseMetaData databaseMetaData = getDatabaseMetaData(connection);
            String[] types = {"VIEW"};
            try (ResultSet rs = databaseMetaData.getTables(connection.getCatalog(),
                    connection.getSchema(), "%", types)) {
                while (rs.next()) {
                    DatabaseTable table = new DatabaseTable();
                    table.setCatalog(rs.getString(1));
                    table.setSchema(rs.getString(2));
                    table.setName(rs.getString(3));
                    table.setType(rs.getString(4));
                    tables.add(table);
                }
            }
            return tables;
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }
    public List<DatabaseTable> getDatabaseTables(Connection connection) throws GUSLErrorException {
        try {
            List<DatabaseTable> tables = new ArrayList<>();

            DatabaseMetaData databaseMetaData = getDatabaseMetaData(connection);
            String[] types = {"TABLE"};
            try (ResultSet rs = databaseMetaData.getTables(connection.getCatalog(),
                    connection.getSchema(), "%", types)) {
                while (rs.next()) {
                    DatabaseTable table = new DatabaseTable();
                    table.setCatalog(rs.getString(1));
                    table.setSchema(rs.getString(2));
                    table.setName(rs.getString(3));
                    table.setType(rs.getString(4));
                    tables.add(table);
                }
            }
            return tables;
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    public List<DatabaseColumn> getDatabaseColumns(DatabaseTable table, Connection connection) throws GUSLErrorException {
        try {
            List<DatabaseColumn> columns = new ArrayList<>();
            DatabaseMetaData databaseMetaData = getDatabaseMetaData(connection);
            try (ResultSet rs = databaseMetaData.getColumns(table.getCatalog(), table.getSchema(), table.getName(), "%")) {
                while (rs.next()) {
                    DatabaseColumn column = new DatabaseColumn();
                    column.setName(rs.getString(4));
                    column.setType(rs.getInt(5));
                    column.setNullable(rs.getInt(11));
                    column.setIsNullable(rs.getString(18));
                    columns.add(column);
                }

                return columns;
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    public List<DatabaseIndex> getIndices(DatabaseTable table, Connection connection) throws GUSLErrorException {
        try {
            List<DatabaseIndex> indices = new ArrayList<>(3);
            try (ResultSet rs = getDatabaseMetaData(connection).getIndexInfo(table.getCatalog(), table.getSchema(), table.getName(),
                    false, false)) {
                while (rs.next()) {
                    DatabaseIndex index = new DatabaseIndex();
                    index.setName(rs.getString(6));
                    index.setColumnName(rs.getString(9));
                    index.setOrder(rs.getString(10));
                    index.setUnique(rs.getBoolean(4));
                    indices.add(index);
                }

                return indices;
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

}
