package gusl.postgres.metadata;

import com.fasterxml.jackson.databind.type.CollectionType;
import gusl.core.tostring.ToString;
import gusl.core.utils.StringUtils;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import gusl.postgres.utils.PostgresUtils;
import lombok.Getter;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dhudson
 * @since 17/05/2021
 */
@Getter
public class FieldTuple {

    private final Field javaField;
    private final PostgresField postgresField;

    private boolean isCollection;
    private CollectionType listType;

    FieldTuple(Field field, PostgresField annotation) {
        javaField = field;
        postgresField = annotation;
        if (List.class.isAssignableFrom(field.getType())) {
            isCollection = true;
            listType = PostgresUtils.OBJECT_MAPPER.getTypeFactory().constructCollectionType(ArrayList.class,
                    (Class<?>) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0]);
        }

    }

    public String getName() {
        if (StringUtils.isNotBlank(postgresField.name())) {
            return postgresField.name();
        } else {
            return javaField.getName();
        }
    }

    public PostgresFieldType getType() {
        return postgresField.type();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
