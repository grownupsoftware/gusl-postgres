package gusl.postgres.dao;

import gusl.annotations.form.page.PagedDAOResponse;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.postgres.connection.PostgresConnection;
import gusl.postgres.errors.PostgresErrors;
import gusl.postgres.metadata.DOMetadata;
import gusl.postgres.metadata.ViewMetadata;
import gusl.postgres.model.PostgresView;
import gusl.query.QueryParams;

import javax.inject.Inject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * @author dhudson
 * @since 25/08/2022
 */
public abstract class AbstractPostgresDAOViewImpl<T> implements PostgresDAOView<T> {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    @Inject
    private PostgresConnection theConnection;

    private ViewMetadata<T> theMetadata;

    private DOMetadata<T> theDBMetadata;

    private final Class<T> theDOClass;

    private boolean isDbView;

    public AbstractPostgresDAOViewImpl(Class<T> clazz) {
        theDOClass = clazz;
    }

    @Override
    public List<T> getAll() throws GUSLErrorException {
        return get(null);
    }

    @Override
    public PagedDAOResponse<T> getAllPaged(QueryParams request) throws GUSLErrorException {
        final List<T> resultList;
        try (PreparedStatement statement = generateSelectStatement(request)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                if (isDbView) {
                    resultList = theDBMetadata.parseList(resultSet, false);
                } else {
                    resultList = theMetadata.parseList(resultSet, false);
                }
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }

        return PagedDAOResponse.<T>builder().content(resultList).queryParams(request).total(getCount(request)).build();
    }

    @Override
    public List<T> get(QueryParams params) throws GUSLErrorException {
        try (PreparedStatement statement = generateSelectStatement(params)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                if (isDbView) {
                    return theDBMetadata.parseList(resultSet, isNull(params) ? false : params.isFirstAndLast());
                } else {
                    return theMetadata.parseList(resultSet, isNull(params) ? false : params.isFirstAndLast());
                }
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    private PreparedStatement generateSelectStatement(QueryParams params) throws GUSLErrorException {
        PreparedStatement statement;
        if (nonNull(params) && params.isFirstAndLast()) {
            statement = theConnection.createScrollInsensitiveStatement(isDbView
                    ? theDBMetadata.getSelectStatement(params)
                    : theMetadata.getSelectStatement(params)
            );
        } else {
            statement = theConnection.createPreparedStatement(isDbView
                    ? theDBMetadata.getSelectStatement(params)
                    : theMetadata.getSelectStatement(params)
            );
        }
        if (isDbView) {
            theDBMetadata.bindQueryStatement(statement, params);
        } else {
            theMetadata.bindQueryStatement(statement, params);
        }
        return statement;
    }

    private int getCount(QueryParams request) throws GUSLErrorException {
        int count;
        try (PreparedStatement statement = theConnection.createPreparedStatement(isDbView
                ? theDBMetadata.buildSelectCountStatement(request)
                : theMetadata.buildSelectCountStatement(request)
        )) {
            if (isDbView) {
                theDBMetadata.bindQueryStatement(statement, request);
            } else {
                theMetadata.bindQueryStatement(statement, request);
            }
            try (ResultSet resultSet = statement.executeQuery()) {
                resultSet.next();
                count = resultSet.getInt("rowcount");
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }

        return count;
    }

    public void config() throws GUSLErrorException {
        PostgresView view = this.getClass().getAnnotation(PostgresView.class);
        isDbView = view.isDbView();

        if (isDbView) {
            theDBMetadata = new DOMetadata<T>(theDOClass, null);
            theDBMetadata.create(view);

        } else {
            theMetadata = new ViewMetadata<>(theDOClass);
            theMetadata.create(view);
        }

    }

    public PostgresConnection getConnection() {
        return theConnection;
    }

    public ViewMetadata<T> getMetadata() {
        return theMetadata;
    }

    public Class<T> getDOClass() {
        return theDOClass;
    }
}
