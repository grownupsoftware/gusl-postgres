package gusl.postgres.dao;

import gusl.core.exceptions.GUSLErrorException;
import gusl.query.QueryParams;

import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * @author dhudson
 * @since 26/04/2023
 */
public class PostgresStreamGenerator<T> implements Iterator<T> {

    private final QueryParams theQueryParams;
    private final Function<QueryParams, List<T>> theMoreFunction;
    private final int theMax;
    private List<T> currentResult;
    private int index;
    private int count;

    PostgresStreamGenerator(int max, QueryParams params, Function<QueryParams, List<T>> getMore) {
        theMax = max;
        theQueryParams = params;
        theMoreFunction = getMore;
    }


    @Override
    public boolean hasNext() {
        return count < theMax;
    }

    @Override
    public T next() {
        if (index >= theQueryParams.getLimit()) {
            // Need to get more
            theQueryParams.setSkip(theQueryParams.getSkip() + theQueryParams.getLimit());
            getNext();
        }
        count++;
        return currentResult.get(index++);
    }

    public Stream<T> stream() throws GUSLErrorException {
        count = 0;
        getNext();
        return generateStream();
    }

    private void getNext() {
        currentResult = theMoreFunction.apply(theQueryParams);
        index = 0;
    }

    Stream<T> generateStream() {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(
                this, Spliterator.ORDERED | Spliterator.NONNULL), false);
    }
}
