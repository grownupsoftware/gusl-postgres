package gusl.postgres.dao;

import gusl.core.exceptions.GUSLErrorException;
import gusl.model.dao.DAOInterface;
import gusl.postgres.delegate.PostgresTransaction;
import gusl.postgres.model.TableJoin;
import gusl.query.QueryParams;
import gusl.query.UpdateParams;

import java.io.File;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

/**
 * @author dhudson
 * @since 23/04/2021
 */
public interface DAOPostgresInterface<T> extends DAOInterface<T> {

    <T> T findAndLock(String id, PostgresTransaction transaction) throws GUSLErrorException;

    void insert(T entity, PostgresTransaction transaction) throws GUSLErrorException;

    void insertBatch(List<T> list, PostgresTransaction transaction) throws GUSLErrorException;

    T update(T entity, PostgresTransaction transaction) throws GUSLErrorException;

    T update(T entity, UpdateParams params, PostgresTransaction transaction) throws GUSLErrorException;

    List<T> get(QueryParams params, boolean debug) throws GUSLErrorException;

    CompletableFuture<List<T>> getAsync(QueryParams params);

    void updateBatch(List<T> list, PostgresTransaction transaction) throws GUSLErrorException;

    void delete(T entity, PostgresTransaction transaction) throws GUSLErrorException;

    void deleteBatch(List<T> list, PostgresTransaction transaction) throws GUSLErrorException;

    int deleteBatch(QueryParams params, PostgresTransaction transaction) throws GUSLErrorException;

    List<T> execute(String sql) throws GUSLErrorException;

    TableJoin createTableJoin(DAOPostgresInterface table2);

    List<T> executeJoin(TableJoin join) throws GUSLErrorException;

    CompletableFuture<List<T>> executeJoinAsync(TableJoin join);

    T upsert(T entity) throws GUSLErrorException;

    T upsert(T entity, PostgresTransaction transaction) throws GUSLErrorException;

    String getSelectStatement();

    void exportCSV(File file, QueryParams params, String... fields) throws GUSLErrorException;

    Stream<T> stream(QueryParams params) throws GUSLErrorException;
}


