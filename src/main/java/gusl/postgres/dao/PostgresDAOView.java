package gusl.postgres.dao;

import gusl.annotations.form.page.PagedDAOResponse;
import gusl.core.exceptions.GUSLErrorException;
import gusl.query.QueryParams;

import java.util.List;

/**
 * @author dhudson
 * @since 25/08/2022
 */
public interface PostgresDAOView<T> {

    List<T> getAll() throws GUSLErrorException;

    PagedDAOResponse<T> getAllPaged(QueryParams request) throws GUSLErrorException;

    List<T> get(QueryParams params) throws GUSLErrorException;

}
