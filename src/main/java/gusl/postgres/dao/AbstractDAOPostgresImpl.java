package gusl.postgres.dao;

import gusl.annotations.form.page.PageRequest;
import gusl.annotations.form.page.PageResponse;
import gusl.annotations.form.page.PageResponseBuilder;
import gusl.annotations.form.page.PagedDAOResponse;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.Utils;
import gusl.loaders.StaticLoader;
import gusl.postgres.connection.PostgresConnection;
import gusl.postgres.delegate.PostgresTransaction;
import gusl.postgres.errors.PostgresErrors;
import gusl.postgres.metadata.DOMetadata;
import gusl.postgres.metadata.FieldTuple;
import gusl.postgres.metadata.JoinMetadata;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import gusl.postgres.model.PostgresTable;
import gusl.postgres.model.TableJoin;
import gusl.postgres.utils.PostgresUtils;
import gusl.query.QueryParams;
import gusl.query.UpdateParams;
import lombok.NonNull;

import javax.inject.Inject;
import java.io.*;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static gusl.postgres.errors.PostgresErrors.RECORD_NOT_FOUND;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public abstract class AbstractDAOPostgresImpl<T> implements DAOPostgresInterface<T> {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    @Inject
    private PostgresConnection theConnection;

    private final Map<String, DOMetadata> theMetadataMap = new ConcurrentHashMap<>(2);

    private String theInsertStatement;
    private String theUpdateStatement;
    private String theDeleteStatement;
    private String theFindStatement;

    private final Class<T> theDOClass;
    private final Class<?> theMixInClass;

    @NonNull
    private Consumer<T> updateConsumer;
    @NonNull
    private Consumer<T> insertConsumer;
    @NonNull
    private Consumer<T> deleteConsumer;

    private DOMetadata theMetadata;

    public AbstractDAOPostgresImpl(Class<T> clazz) {
        this(clazz, null);
    }

    private static final int BATCH_SIZE = 2000;

    public AbstractDAOPostgresImpl(Class<T> clazz, Class<?> mixInClass) {
        theDOClass = clazz;
        theMixInClass = mixInClass;
        updateConsumer = dummyConsumer();
        insertConsumer = dummyConsumer();
        deleteConsumer = dummyConsumer();
    }

    public Class<T> getDOClass() {
        return theDOClass;
    }

    @Override
    public T insert(T entity) throws GUSLErrorException {
        insert(entity, theConnection.getAutoCommitConnection());
        return entity;
    }

    @Override
    public void insert(T entity, PostgresTransaction transaction) throws GUSLErrorException {
        insert(entity, transaction.getConnection());
    }

    private void insert(T entity, Connection connection) throws GUSLErrorException {
        try (PreparedStatement statement = connection.prepareStatement(theInsertStatement)) {
            insert(entity, statement);
            statement.executeUpdate();
            executeConsumer(insertConsumer, entity);
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public List<T> insertBatch(List<T> list) throws GUSLErrorException {
        insertBatch(list, theConnection.getAutoCommitConnection());
        return list;
    }

    @Override
    public void insertBatch(List<T> list, PostgresTransaction transaction) throws GUSLErrorException {
        insertBatch(list, transaction.getConnection());
    }

    private void insertBatch(List<T> list, Connection connection) throws GUSLErrorException {
        try (PreparedStatement statement = connection.prepareStatement(theInsertStatement)) {
            for (T entity : list) {
                insert(entity, statement);
                statement.addBatch();
            }
            statement.executeBatch();
        } catch (SQLException ex) {
            logger.error("Error", ex);
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    private void insert(T entity, PreparedStatement statement) throws GUSLErrorException {
        // Set any fields if required
        theMetadata.updateInsert(entity);
        for (int i = 0; i < theMetadata.getFieldCount(); i++) {
            bindStatement(statement, theMetadata.getFieldTupleAt(i), i + 1, entity);
        }
    }

    @Override
    public T update(T entity) throws GUSLErrorException {
        return update(entity, theConnection.getAutoCommitConnection());
    }

    @Override
    public T update(T entity, PostgresTransaction transaction) throws GUSLErrorException {
        return update(entity, transaction.getConnection());
    }

    private T update(T entity, Connection connection) throws GUSLErrorException {
        try (PreparedStatement statement = connection.prepareStatement(theUpdateStatement)) {
            logger.debug("Update {}", statement);
            update(entity, statement);
            statement.executeUpdate();
            executeConsumer(updateConsumer, entity);
            return entity;
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public void updateBatch(List<T> list) throws GUSLErrorException {
        updateBatch(list, theConnection.getAutoCommitConnection());
    }

    @Override
    public void updateBatch(List<T> list, PostgresTransaction transaction) throws GUSLErrorException {
        updateBatch(list, transaction.getConnection());
    }

    private void updateBatch(List<T> list, Connection connection) throws GUSLErrorException {
        try (PreparedStatement statement = connection.prepareStatement(theUpdateStatement)) {
            logger.debug("Update {}", statement);
            for (T entity : list) {
                update(entity, statement);
                statement.addBatch();
            }
            statement.executeBatch();
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    private void update(T entity, PreparedStatement statement) throws GUSLErrorException {
        // Set any fields if required
        theMetadata.updateUpdate(entity);
        int sqlIndex = 1;
        for (int i = 0; i < theMetadata.getFieldCount(); i++) {
            FieldTuple tuple = theMetadata.getFieldTupleAt(i);

            if (tuple.getJavaField().equals(theMetadata.getJavaPrimaryKey())) {
                // Not yet
                continue;
            }
            bindStatement(statement, tuple, sqlIndex++, entity);
        }

        // Where primary key
        bindStatement(statement, theMetadata.getJavaPrimaryKey(), theMetadata.getPostgresPrimaryKey(), sqlIndex, entity);
    }

    @Override
    public T update(T entity, UpdateParams params) throws GUSLErrorException {
        return update(entity, params, theConnection.getAutoCommitConnection());
    }

    @Override
    public T update(T entity, UpdateParams params, PostgresTransaction transaction) throws GUSLErrorException {
        return update(entity, params, transaction.getConnection());
    }

    private T update(T entity, UpdateParams params, Connection connection) throws GUSLErrorException {
        if (params == null || params.getUpdateFields() == null || params.getUpdateFields().isEmpty()) {
            throw new GUSLErrorException(PostgresErrors.EMPTY_QUERY_PARAMS.getError());
        }

        try (PreparedStatement statement = connection.prepareStatement(buildCustomUpdate(params))) {
            // Set any fields if required
            theMetadata.updateUpdate(entity);
            int sqlIndex = 1;
            for (String fieldName : params.getUpdateFields()) {
                FieldTuple tuple = theMetadata.getFieldTupleFor(fieldName);
                bindStatement(statement, tuple, sqlIndex++, entity);
            }
            if (theMetadata.getUpdatedField() != null) {
                FieldTuple tuple = theMetadata.getFieldTupleFor(theMetadata.getUpdatedField().getName());
                bindStatement(statement, tuple, sqlIndex++, entity);
            }
            // Where clause
            bindStatement(statement, theMetadata.getJavaPrimaryKey(), theMetadata.getPostgresPrimaryKey(), sqlIndex, entity);
            try (ResultSet resultSet = statement.executeQuery()) {
                T thing = (T) theMetadata.parseSingle(resultSet);
                executeConsumer(updateConsumer, thing);
                return thing;
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    private String buildCustomUpdate(UpdateParams params) throws GUSLErrorException {
        StringBuilder builder = new StringBuilder(128);
        builder.append("UPDATE ");
        builder.append(theMetadata.getTableName());
        builder.append(" SET ");

        int fields = 0;
        for (String fieldName : params.getUpdateFields()) {
            if (fields++ > 0) {
                builder.append(",");
            }
            // If this doesn't exist then it will fail
            theMetadata.findFieldByName(fieldName);
            // Check to see if primary key
            builder.append(fieldName);
            builder.append("=?");
        }

        // Let's see if there is a dateUpdated field
        if (theMetadata.getUpdatedField() != null) {
            Field updateField = theMetadata.getUpdatedField();
            // Ok there is something that we need to do
            if (!params.getUpdateFields().contains(updateField.getName())) {
                // Its hasn't been specified, so lets add it
                builder.append(", ");
                builder.append(updateField.getName());
                builder.append("=?");
            }
        }
        builder.append(" WHERE ");
        builder.append(theMetadata.getJavaPrimaryKey().getName());
        builder.append(" =? RETURNING *;");

        logger.debug("buildCustomUpdate: {} ", builder.toString());

        return builder.toString();
    }

    @Override
    public void delete(T entity) throws GUSLErrorException {
        delete(entity, theConnection.getAutoCommitConnection());
    }

    @Override
    public void delete(T entity, PostgresTransaction transaction) throws GUSLErrorException {
        delete(entity, transaction.getConnection());
    }

    private void delete(T entity, Connection connection) throws GUSLErrorException {
        try (PreparedStatement statement = connection.prepareStatement(theDeleteStatement)) {
            bindStatement(statement, theMetadata.getJavaPrimaryKey(), theMetadata.getPostgresPrimaryKey(), 1, entity);
            statement.executeUpdate();
            executeConsumer(deleteConsumer, entity);
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public void deleteBatch(List<T> list) throws GUSLErrorException {
        deleteBatch(list, theConnection.getAutoCommitConnection());
    }

    @Override
    public void deleteBatch(List<T> list, PostgresTransaction transaction) throws GUSLErrorException {
        deleteBatch(list, transaction.getConnection());
    }

    private void deleteBatch(List<T> list, Connection connection) throws GUSLErrorException {
        try (PreparedStatement statement = connection.prepareStatement(theDeleteStatement)) {
            for (T entity : list) {
                bindStatement(statement, theMetadata.getJavaPrimaryKey(), theMetadata.getPostgresPrimaryKey(), 1, entity);
                statement.addBatch();
            }
            statement.executeBatch();
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public int deleteBatch(QueryParams params) throws GUSLErrorException {
        return deleteBatch(params, theConnection.getAutoCommitConnection());
    }

    @Override
    public int deleteBatch(QueryParams params, PostgresTransaction transaction) throws GUSLErrorException {
        return deleteBatch(params, transaction.getConnection());
    }

    private int deleteBatch(QueryParams params, Connection connection) throws GUSLErrorException {
        String sql = theMetadata.addQueryParams("DELETE FROM " + theMetadata.getTableName() + " ", params);
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            theMetadata.bindQueryStatement(statement, params);
            return statement.executeUpdate();
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public List<T> get(QueryParams params) throws GUSLErrorException {
        if (params != null && Utils.hasElements(params.getDistinct())) {
            StringBuilder builder = new StringBuilder(100);
            builder.append("SELECT DISTINCT ");
            builder.append(String.join(", ", params.getDistinct()));
            builder.append(" FROM ");
            builder.append(theMetadata.getTableName());

            String sql = theMetadata.addQueryParams(builder.toString(), params);
            try {
                PreparedStatement statement = theConnection.createPreparedStatement(sql);
                theMetadata.bindQueryStatement(statement, params);
                try (ResultSet resultSet = statement.executeQuery()) {
                    return theMetadata.parseList(resultSet, false);
                }
            } catch (SQLException ex) {
                throw PostgresErrors.SQL_ERROR.generateException(ex);
            }
        }
        return get(params, false);
    }

    @Override
    public List<T> get(QueryParams params, boolean debug) throws GUSLErrorException {
        try (PreparedStatement statement = generateSelectStatement(params, theMetadata)) {
            if (logger.isDebugEnabled() || debug) {
                logger.info("Get {}", statement);
            }
            try (ResultSet resultSet = statement.executeQuery()) {
                return theMetadata.parseList(resultSet, isNull(params) ? false : params.isFirstAndLast());
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public Optional<T> getOne(QueryParams params) throws GUSLErrorException {
        try (PreparedStatement statement = generateSelectStatement(params, theMetadata)) {
            logger.debug("GetOne {}", statement);
            try (ResultSet resultSet = statement.executeQuery()) {
                return populateOptional(resultSet);
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public List<T> getAll() throws GUSLErrorException {
        return get(null);
    }

    @Override
    public CompletableFuture<List<T>> getAsync(QueryParams params) {
        CompletableFuture<List<T>> future = new CompletableFuture<>();
        theConnection.submit(() -> {
            try {
                future.complete(get(params));
            } catch (Throwable throwable) {
                future.completeExceptionally(throwable);
            }
        });
        return future;
    }

    @Override
    public <VIEW> List<VIEW> getView(Class<VIEW> clazz, QueryParams params) throws GUSLErrorException {
        DOMetadata<VIEW> metadata;
        if (theMetadataMap.containsKey(clazz.getName())) {
            metadata = theMetadataMap.get(clazz.getName());
        } else {
            metadata = theMetadata.createView(clazz);
            theMetadataMap.put(clazz.getName(), metadata);
        }

        try (PreparedStatement statement = generateSelectStatement(params, metadata)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                return metadata.parseList(resultSet, params.isFirstAndLast());
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    private PreparedStatement generateSelectStatement(QueryParams params, DOMetadata metadata) throws GUSLErrorException {
        PreparedStatement statement;
        if (nonNull(params) && params.isFirstAndLast()) {
            statement = theConnection.createScrollInsensitiveStatement(metadata.getSelectStatement(params));
        } else {
            statement = theConnection.createPreparedStatement(metadata.getSelectStatement(params));
        }
        metadata.bindQueryStatement(statement, params);
        return statement;
    }

    public String getSelectStatement() {
        return theMetadata.getSelectStatement();
    }

    @Override
    public Optional<T> findRecord(T entity) throws GUSLErrorException {
        try (PreparedStatement statement = theConnection.createPreparedStatement(theFindStatement)) {
            logger.debug("Find {}", statement);

            bindStatement(statement, theMetadata.getJavaPrimaryKey(), theMetadata.getPostgresPrimaryKey(), 1, entity);
            try (ResultSet resultSet = statement.executeQuery()) {
                return populateOptional(resultSet);
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public T upsert(T entity) throws GUSLErrorException {
        return upsert(entity, theConnection.getAutoCommitConnection());
    }

    @Override
    public T upsert(T entity, PostgresTransaction transaction) throws GUSLErrorException {
        return upsert(entity, transaction.getConnection());
    }

    private T upsert(T entity, Connection connection) throws GUSLErrorException {
        if (findRecord(entity).isPresent()) {
            update(entity, connection);
        } else {
            insert(entity, connection);
        }
        return entity;
    }

    private Optional<T> populateOptional(ResultSet resultSet) throws GUSLErrorException {
        T result = (T) theMetadata.parseSingle(resultSet);

        if (result == null) {
            return Optional.empty();
        }

        return Optional.of(result);
    }

    @Override
    public <T> T findRecord(String id) throws GUSLErrorException {
        return findRecord(id, theFindStatement, theConnection.getAutoCommitConnection());
    }

    @Override
    public <T> T findAndLock(String id, PostgresTransaction transaction) throws GUSLErrorException {
        return findRecord(id, theFindStatement.replace(";", " FOR UPDATE;"), transaction.getConnection());
    }

    private <T> T findRecord(String id, String sql, Connection connection) throws GUSLErrorException {
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            PostgresUtils.bindStatementValue(statement, theMetadata.getJavaPrimaryKey(), theMetadata.getPostgresPrimaryKey(), 1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                T result = (T) theMetadata.parseSingle(resultSet);
                if (result == null) {
                    throw RECORD_NOT_FOUND.generateExceptionNoLogging(id).setSuppressStack(true);
                }
                return result;
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public List<T> execute(String sql) throws GUSLErrorException {
        try (PreparedStatement statement = theConnection.createPreparedStatement(sql)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                return theMetadata.parseList(resultSet, false);
            }
        } catch (SQLException ex) {
            logger.error("Error", ex);
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public TableJoin createTableJoin(DAOPostgresInterface table2) {
        TableJoin join = new TableJoin();
        // Yuk, lets see if we can create another interface here
        join.setMetadata(((AbstractDAOPostgresImpl) table2).getMetadata());
        return join;
    }

    @Override
    public List<T> executeJoin(TableJoin join) throws GUSLErrorException {

        JoinMetadata joinMetadata = new JoinMetadata(theMetadata, join);
        logger.debug("JOIN {}", joinMetadata.getStatement());

        try (PreparedStatement statement = theConnection.createPreparedStatement(joinMetadata.getStatement())) {
            joinMetadata.bindQueryStatement(statement);
            try (ResultSet resultSet = statement.executeQuery()) {
                return theMetadata.parseList(resultSet, false);
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public CompletableFuture<List<T>> executeJoinAsync(TableJoin join) {
        CompletableFuture future = new CompletableFuture<>();
        theConnection.submit(() -> {
            try {
                future.complete(executeJoin(join));
            } catch (Throwable throwable) {
                future.completeExceptionally(throwable);
            }
        });
        return future;
    }

    public void config() throws GUSLErrorException {
        theMetadata = new DOMetadata(theDOClass, theMixInClass);
        theMetadata.create(this.getClass().getAnnotation(PostgresTable.class));

        theDeleteStatement = createDeleteStatement();
        theFindStatement = createFindStatement();
        theInsertStatement = createInsertStatement();
        theUpdateStatement = createUpdateStatement();
    }

    private String createFindStatement() {
        StringBuilder builder = new StringBuilder(theMetadata.getSelectStatement());
        appendPrimaryKeyClause(builder);
        return builder.toString();
    }

    private String createDeleteStatement() {
        StringBuilder builder = new StringBuilder(128);
        builder.append("DELETE FROM ");
        builder.append(theMetadata.getTableName());
        appendPrimaryKeyClause(builder);
        return builder.toString();
    }

    private void appendPrimaryKeyClause(StringBuilder builder) {
        builder.append(" WHERE ");
        builder.append(theMetadata.getJavaPrimaryKey().getName());
        builder.append(" =?;");
    }

    private String createInsertStatement() {
        StringBuilder builder = new StringBuilder(128);

        builder.append("INSERT INTO ");
        builder.append(theMetadata.getTableName());
        builder.append(" (");
        StringBuilder values = new StringBuilder(128);
        values.append("VALUES (");
        for (int i = 0; i < theMetadata.getFieldCount(); i++) {
            FieldTuple tuple = theMetadata.getFieldTupleAt(i);

            if (i > 0) {
                builder.append(",");
                values.append(",");
            }

            builder.append(tuple.getName());

            values.append("?");
            if (tuple.getType() == PostgresFieldType.JSONB) {
                values.append("::JSONB");
            }
        }

        builder.append(") ");
        builder.append(values);
        builder.append(");");
        return builder.toString();
    }

    private String createUpdateStatement() {
        StringBuilder builder = new StringBuilder(128);
        builder.append("UPDATE ");
        builder.append(theMetadata.getTableName());
        builder.append(" SET ");
        int fields = 0;
        for (int i = 0; i < theMetadata.getFieldCount(); i++) {
            Field field = theMetadata.getJavaField(i);
            if (field.equals(theMetadata.getJavaPrimaryKey())) {
                continue;
            }

            if (fields++ > 0) {
                builder.append(",");
            }

            builder.append(field.getName());
            builder.append("=?");
        }

        appendPrimaryKeyClause(builder);
        return builder.toString();
    }

    private void bindStatement(PreparedStatement statement, FieldTuple tuple, int sqlIndex, T entity) throws GUSLErrorException {
        bindStatement(statement, tuple.getJavaField(), tuple.getPostgresField(), sqlIndex, entity);
    }

    private void bindStatement(PreparedStatement statement, Field javaField, PostgresField postgresField, int sqlIndex, T entity) throws GUSLErrorException {
        try {
            PostgresUtils.bindStatementValue(statement, javaField, postgresField, sqlIndex, javaField.get(entity));
        } catch (GUSLErrorException ex) {
            logger.error("Failed to bind: statement:{} javaField:{} postgresField:{} sqlindex:{} entity:{}", statement, javaField, postgresField, sqlIndex, entity);
            throw ex;
        } catch (IllegalAccessException ex) {
            logger.error("Failed to bind: statement:{} javaField:{} postgresField:{} sqlindex:{} entity:{}", statement, javaField, postgresField, sqlIndex, entity);
            throw PostgresErrors.BIND_ERROR.generateException(ex, ex.getLocalizedMessage());
        }
    }

    @Override
    public Stream<T> stream(QueryParams params) throws GUSLErrorException {
        if (params.getLimit() <= 0) {
            params.setLimit(BATCH_SIZE);
        }

        try {
            int count = getCount(params);

            PostgresStreamGenerator<T> generator = new PostgresStreamGenerator<T>(count, params, queryParams -> {
                try {
                    return get(params, false);
                } catch (GUSLErrorException ex) {
                    logger.warn("Error during streaming", ex);
                }
                return null;
            });

            return generator.stream();

        } catch (GUSLErrorException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public PagedDAOResponse<T> getAllPaged(QueryParams request) throws GUSLErrorException {
        final List<T> resultList;
        try (PreparedStatement statement = generateSelectStatement(request, theMetadata)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                resultList = theMetadata.parseList(resultSet, false);
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }

        return PagedDAOResponse.<T>builder().content(resultList).queryParams(request).total(getCount(request)).build();
    }

    private int getCount(QueryParams request) throws GUSLErrorException {
        int count;
        try (PreparedStatement statement = theConnection.createPreparedStatement(theMetadata.buildSelectCountStatement(request))) {
            theMetadata.bindQueryStatement(statement, request);
            try (ResultSet resultSet = statement.executeQuery()) {
                resultSet.next();
                count = resultSet.getInt("rowcount");
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }

        return count;
    }

    // Used by evolutions and fixer
    public void setConnection(PostgresConnection connection) {
        theConnection = connection;
    }

    @Override
    public int loadResource(String resource) throws GUSLErrorException {
        List<T> staticData = StaticLoader.loadResource(resource, getDOClass());
        insertBatch(staticData);
        return staticData.size();
    }

    @Override
    public int loadFile(File file) throws GUSLErrorException {
        List<T> staticData = StaticLoader.loadFile(file, getDOClass());
        insertBatch(staticData);
        return staticData.size();
    }

    @Override
    public void exportCSV(File file, QueryParams params, String... fields) throws GUSLErrorException {
        if (params == null) {
            params = new QueryParams();
        }

        List<String> headers;
        if (fields == null || fields.length == 0) {
            headers = theMetadata.getFieldNames();
        } else {
            headers = Arrays.asList(fields);
        }

        try {
            try (FileOutputStream fos = new FileOutputStream(file)) {
                try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos))) {

                    List<T> data = get(params);
                    bw.write(String.join(",", headers));
                    bw.newLine();

                    for (T entity : data) {
                        Iterator<String> it = headers.iterator();
                        while (it.hasNext()) {
                            String name = it.next();
                            FieldTuple tuple = theMetadata.getFieldTupleFor(name);
                            Object value = tuple.getJavaField().get(entity);
                            if (value != null) {
                                String strValue = value.toString();
                                if (tuple.getPostgresField().type() == PostgresFieldType.JSONB) {
                                    quote("|", strValue, bw);
                                } else {
                                    if (value.toString().contains(",")) {
                                        strValue = strValue.replace(",", "\\,");
                                    }
                                    bw.write(strValue);
                                }
                            }
                            if (it.hasNext()) {
                                bw.write(",");
                            }
                        }
                        bw.newLine();
                    }
                }
            }

        } catch (IOException | IllegalAccessException ex) {
            logger.warn("Unable to export", ex);
            throw GUSLErrorException.of(ex.getMessage());
        }

    }

    private void quote(String quote, String value, BufferedWriter bw) throws IOException {
        bw.write(quote);
        bw.write(value);
        bw.write(quote);
    }

    // Consumers
    private Consumer<T> dummyConsumer() {
        return result -> {
        };
    }

    private void executeConsumer(Consumer<T> consumer, T entity) {
        try {
            consumer.accept(entity);
        } catch (Throwable throwable) {
            logger.warn("Error running consumer", throwable);
        }
    }

    public void setUpdateConsumer(Consumer<T> updateConsumer) {
        this.updateConsumer = updateConsumer;
    }

    public void setInsertConsumer(Consumer<T> insertConsumer) {
        this.insertConsumer = insertConsumer;
    }

    public void setDeleteConsumer(Consumer<T> deleteConsumer) {
        this.deleteConsumer = deleteConsumer;
    }

    DOMetadata<T> getMetadata() {
        return theMetadata;
    }


    // ------------------------------------- following here until supernova is migrated to QueryParams

    public PageResponse<T> getAllPaged(PageRequest request) throws GUSLErrorException {
        final List<T> resultList;
        try (PreparedStatement statement = generateSelectStatement(request, theMetadata)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                resultList = theMetadata.parseList(resultSet, false);
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }

        final PageResponseBuilder<T> responsePage = PageResponseBuilder.<T>builder().offset(request.getOffset()).perPage(request.getPerPage()).total(getCount(request, theMetadata)).orderParamTieBreaker(request.getOrderParamTieBreaker()).orderDirection(request.getOrderDirection()).orderParam(request.getOrderParam()).content(resultList).queryConditions(request.getQueryConditions()).after(request.getAfter()).before(request.getBefore()).convertTo(request.getConvertTo()).indexClass(request.getIndexClass()).last(request.getLast()).build();

        return responsePage;

    }

    private int getCount(PageRequest request, DOMetadata metadata) throws GUSLErrorException {
        int count = 0;
        try (PreparedStatement statement = theConnection.createPreparedStatement(metadata.buildSelectCountStatement(request))) {
            metadata.bindQueryStatement(statement, request);
            try (ResultSet resultSet = statement.executeQuery()) {
                resultSet.next();
                count = resultSet.getInt("rowcount");
            }
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }

        return count;
    }

    private PreparedStatement generateSelectStatement(PageRequest request, DOMetadata metadata) throws GUSLErrorException {
        PreparedStatement statement = theConnection.createPreparedStatement(metadata.getSelectStatement(request));
        metadata.bindQueryStatement(statement, request);
        return statement;
    }

    public void setPostgresConnection(PostgresConnection connection) {
        theConnection = connection;
    }
}
