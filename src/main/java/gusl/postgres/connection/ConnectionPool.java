package gusl.postgres.connection;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.json.JsonUtils;
import gusl.postgres.config.PostgresConfig;
import gusl.postgres.errors.PostgresErrors;
import gusl.postgres.utils.PostgresUtils;
import lombok.CustomLog;
import org.postgresql.ds.PGSimpleDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @author dhudson
 * @since 07/05/2021
 */
@CustomLog
public class ConnectionPool {

    private static final int DEFAULT_MAX_CONNECTIONS = 10;
    private static final int DEFAULT_THRESHOLD = 5;

    static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
            logger.fatal("Postgres not found", ex);
        }
    }

    private final Deque<Connection> connectionPool;
    private final Deque<Connection> usedConnections;
    private final Connection[] theAutoCommitConnections;
    private final PostgresConfig theConfig;
    private final PGSimpleDataSource theDataSource;
    private final int MAX_CONNECTIONS;
    private int autoConnectionCount = 0;

    ConnectionPool(PostgresConfig config) throws GUSLErrorException {
        theConfig = config;
        if (theConfig.getMaxConnections() == null) {
            theConfig.setMaxConnections(DEFAULT_MAX_CONNECTIONS);
        }
        if (theConfig.getPrepareThreshold() == null) {
            theConfig.setPrepareThreshold(DEFAULT_THRESHOLD);
        }

        MAX_CONNECTIONS = theConfig.getMaxConnections();
        connectionPool = new ArrayDeque<>(MAX_CONNECTIONS);
        usedConnections = new ArrayDeque<>(MAX_CONNECTIONS);

        theDataSource = PostgresUtils.createDatasourceFor(config);
        // Postgres Connections are thread safe and will queue statements.
        theAutoCommitConnections = new Connection[MAX_CONNECTIONS];
        for (int i = 0; i < MAX_CONNECTIONS; i++) {
            theAutoCommitConnections[i] = createConnection(true);
        }
    }

     public Connection getAutoCommitConnection() {
        return theAutoCommitConnections[autoConnectionCount++ % MAX_CONNECTIONS];
    }

    public synchronized Connection getTransactionConnection() throws GUSLErrorException {
        Connection connection;
        if (connectionPool.isEmpty()) {
            if (usedConnections.size() >= theConfig.getMaxConnections()) {
                throw new GUSLErrorException(PostgresErrors.MAX_CONNECTIONS.getError());
            }
            connection = createConnection(false);
        } else {
            connection = connectionPool.pop();
        }

        usedConnections.push(connection);
        return connection;
    }

    public synchronized void releaseTransactionConnection(Connection connection) {
        usedConnections.remove(connection);
        connectionPool.push(connection);
    }

    public int getPoolSize() {
        return usedConnections.size() + connectionPool.size();
    }

    public PostgresConfig getConfig() {
        return theConfig;
    }

    private Connection createConnection(boolean autoCommit) throws GUSLErrorException {
        try {
            Connection connection = theDataSource.getConnection();
            connection.setAutoCommit(autoCommit);
            return connection;
        } catch (SQLException ex) {
            logger.warn("Postgres config: {}", JsonUtils.prettyPrint(theConfig));
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public String toString() {
        return "Connection Pool- Used: " + usedConnections.size() + ", Free: " + connectionPool.size();
    }
}
