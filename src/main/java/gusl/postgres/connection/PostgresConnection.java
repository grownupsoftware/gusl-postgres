package gusl.postgres.connection;

import gusl.core.exceptions.GUSLErrorException;
import gusl.postgres.application.DatabaseColumn;
import gusl.postgres.application.DatabaseIndex;
import gusl.postgres.application.DatabaseTable;
import gusl.postgres.config.PostgresConfig;
import org.jvnet.hk2.annotations.Contract;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

/**
 * @author dhudson
 * @since 23/04/2021
 */
@Contract
public interface PostgresConnection {

    void connect(PostgresConfig config) throws GUSLErrorException;

    DatabaseMetaData getDatabaseMetaData() throws GUSLErrorException;

    List<DatabaseTable> getDatabaseTables() throws GUSLErrorException;

    List<DatabaseTable> getDatabaseViews() throws GUSLErrorException;

    List<DatabaseColumn> getDatabaseColumns(DatabaseTable table) throws GUSLErrorException;

    List<DatabaseIndex> getIndices(DatabaseTable table) throws GUSLErrorException;

    PreparedStatement createPreparedStatement(String sql, Connection connection) throws GUSLErrorException;

    PreparedStatement createPreparedStatement(String sql) throws GUSLErrorException;

    PreparedStatement createScrollInsensitiveStatement(String sql) throws GUSLErrorException;

    Statement createStatement() throws GUSLErrorException;

    Future<?> submit(Runnable task);

    Connection getAutoCommitConnection();

    ConnectionPool getConnectionPool();

    String getDatabaseName();

    void waitForStartup(Duration duration) throws TimeoutException;

}
