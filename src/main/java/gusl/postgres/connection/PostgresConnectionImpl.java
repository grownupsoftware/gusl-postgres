package gusl.postgres.connection;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.executors.NamedThreadFactory;
import gusl.core.utils.IOUtils;
import gusl.postgres.application.DatabaseColumn;
import gusl.postgres.application.DatabaseIndex;
import gusl.postgres.application.DatabaseTable;
import gusl.postgres.config.PostgresConfig;
import gusl.postgres.errors.PostgresErrors;
import gusl.postgres.metadata.AbstractMetadataConnection;
import lombok.CustomLog;
import lombok.NoArgsConstructor;
import org.jvnet.hk2.annotations.Service;

import java.sql.*;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

/**
 * @author dhudson
 * @since 23/04/2021
 */
@Service
@CustomLog
@NoArgsConstructor
public class PostgresConnectionImpl extends AbstractMetadataConnection implements PostgresConnection {

    private ExecutorService theExecutorService;
    private final int DEFAULT_THREADS = 5;
    private int nThreads;

    private ConnectionPool theConnectionPool;
    private PostgresConfig theConfig;

    @Override
    public void connect(PostgresConfig config) throws GUSLErrorException {
        theConfig = config;
        theConnectionPool = new ConnectionPool(config);

        nThreads = DEFAULT_THREADS;
        if (config.getAsyncThreads() != null) {
            nThreads = config.getAsyncThreads();
        }
    }

    @Override
    public PreparedStatement createPreparedStatement(String sql) throws GUSLErrorException {
        return createPreparedStatement(sql, theConnectionPool.getAutoCommitConnection());
    }

    @Override
    public PreparedStatement createPreparedStatement(String sql, Connection connection) throws GUSLErrorException {
        try {
            return connection.prepareStatement(sql);
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public PreparedStatement createScrollInsensitiveStatement(String sql) throws GUSLErrorException {
        try {
            return theConnectionPool.getAutoCommitConnection().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    @Override
    public Statement createStatement() throws GUSLErrorException {
        try {
            return theConnectionPool.getAutoCommitConnection().createStatement();
        } catch (SQLException ex) {
            throw PostgresErrors.SQL_ERROR.generateException(ex);
        }
    }

    /**
     * The reason for doing this is so that the Executor Pool is not created until something Async is required.
     * JDBC is *not* async by nature.
     *
     * @param task
     * @return
     */
    @Override
    public synchronized Future<?> submit(Runnable task) {
        if (theExecutorService == null) {
            theExecutorService = Executors.newFixedThreadPool(nThreads, new NamedThreadFactory("Postgres Async"));
        }

        return theExecutorService.submit(task);
    }

    @Override
    public DatabaseMetaData getDatabaseMetaData() throws GUSLErrorException {
        return getDatabaseMetaData(getAutoCommitConnection());
    }

    @Override
    public List<DatabaseTable> getDatabaseTables() throws GUSLErrorException {
        return getDatabaseTables(getAutoCommitConnection());
    }

    @Override
    public List<DatabaseTable> getDatabaseViews() throws GUSLErrorException {
        return getDatabaseViews(getAutoCommitConnection());
    }

    @Override
    public List<DatabaseColumn> getDatabaseColumns(DatabaseTable table) throws GUSLErrorException {
        return getDatabaseColumns(table, getAutoCommitConnection());
    }

    @Override
    public List<DatabaseIndex> getIndices(DatabaseTable table) throws GUSLErrorException {
        return getIndices(table, getAutoCommitConnection());
    }

    @Override
    public Connection getAutoCommitConnection() {
        return theConnectionPool.getAutoCommitConnection();
    }

    @Override
    public ConnectionPool getConnectionPool() {
        return theConnectionPool;
    }

    @Override
    public String getDatabaseName() {
        return theConfig.getDatabase();
    }

    @Override
    public void waitForStartup(Duration duration) throws TimeoutException {
        logger.info("Waiting for the Postgres Connection to start {}", theConfig);
        String[] hosts = theConfig.getServerNames();
        IOUtils.waitForService(hosts[0], theConfig.getPort(), duration, logger);
    }
}
