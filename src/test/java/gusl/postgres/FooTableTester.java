package gusl.postgres;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.utils.IdGenerator;
import gusl.postgres.config.PostgresConfig;
import gusl.postgres.connection.PostgresConnectionImpl;
import gusl.query.QueryParams;
import lombok.CustomLog;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author dhudson
 * @since 12/09/2022
 */
@CustomLog
public class FooTableTester {

    private static PostgresConnectionImpl connection = new PostgresConnectionImpl();
    private static FooTable fooTable = new FooTable();

    @BeforeClass
    public static void createConnection() throws GUSLErrorException {
        PostgresConfig config = new PostgresConfig();
        config.setDatabase("alvar");
        config.setPassword("password");
        config.setUsername("postgres");
        config.setConnectionPoints("localhost");

        connection.connect(config);
        fooTable.setConnection(connection);
        fooTable.config();

        IdGenerator.setForTesting();
    }

    @Test
    public void testList() throws GUSLErrorException {
        String key = IdGenerator.generateUniqueNodeIdAsString();
        Foo foo = new Foo();
        foo.setId(key);

        List<String> accounts = new ArrayList<>(2);
        accounts.add("Foo");
        accounts.add("Bar");
        foo.setAccounts(accounts);

        List<Bar> bars = new ArrayList<>(3);
        bars.add(Bar.builder().id(1L).name("Bill").build());
        bars.add(Bar.builder().id(2L).name("Ben").build());
        foo.setBars(bars);

        fooTable.insert(foo);

        Foo bar = fooTable.findRecord(key);
        logger.info("{}", bar);
    }

    @Test
    public void testDefaultValues() throws GUSLErrorException {

        String key = IdGenerator.generateUniqueNodeIdAsString();
        Foo foo = new Foo();
        foo.setId(key);

        fooTable.insert(foo);

        Foo fighter = fooTable.findRecord(key);

        logger.info("{}", fighter);

        fighter = new Foo();
        fighter.setId(IdGenerator.generateUniqueNodeIdAsString());

        Optional<Foo> pat = fooTable.findRecord(fighter);

        logger.info("{}", pat);
    }

    @Test
    public void distinctTest() throws GUSLErrorException {

        QueryParams queryParams = new QueryParams();
        queryParams.addDistinct("id");
        queryParams.addDistinct("staticData");
        queryParams.addMust("populatedBoolean", Boolean.TRUE);
        List<Foo> foos = fooTable.get(queryParams);
        logger.info("{}", foos);
    }

    @Test
    public void shouldTest() throws GUSLErrorException {


        for (int i = 0; i < 20; i++) {
            Foo foo = new Foo();
            foo.setId(IdGenerator.generateUniqueNodeIdAsString());
            if (i % 3 == 0) {
                foo.setPopulatedBoolean(Boolean.TRUE);
                foo.setStaticData("bye");
            }

            fooTable.insert(foo);
        }

        QueryParams queryParams = new QueryParams();
        queryParams.addShould("staticData", "bye");
        queryParams.addShould("staticData", "Hello");

        logger.info("{}", fooTable.get(queryParams));
    }

    @Test
    public void deleteTable() throws GUSLErrorException {

        fooTable.deleteBatch(new QueryParams());

        logger.info("Have left {}", fooTable.getAll().size());
    }

    @Test
    public void doubleTester() throws GUSLErrorException {
        for (Foo fighter : fooTable.getAll()) {
            fighter.setPrice(Double.valueOf("1234.5678"));
            fighter.setAsk(new BigDecimal("8765.4321"));
            fooTable.update(fighter);
        }

        logger.info("{}", fooTable.getAll());
    }
}
