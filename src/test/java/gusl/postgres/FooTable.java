package gusl.postgres;

import gusl.postgres.dao.AbstractDAOPostgresImpl;
import gusl.postgres.model.PostgresTable;

/**
 * @author dhudson
 * @since 13/04/2022
 */
@PostgresTable(table = "foo", version = Foo.VERSION, type = Foo.class)
public class FooTable extends AbstractDAOPostgresImpl<Foo> {

    public FooTable() {
        super(Foo.class);
    }
}
