package gusl.postgres;

import gusl.postgres.dao.AbstractDAOPostgresImpl;
import gusl.postgres.model.PostgresTable;

/**
 * @author dhudson
 * @since 17/05/2022
 */
@PostgresTable(table = "secret", version = Secret.VERSION, type = Secret.class)
public class SecretTable extends AbstractDAOPostgresImpl<Secret> {

    public SecretTable() {
        super(Secret.class);
    }
}
