package gusl.postgres;

import gusl.core.security.ObfuscateMethod;
import gusl.core.tostring.ToString;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.Getter;
import lombok.Setter;

/**
 * @author dhudson
 * @since 17/05/2022
 */
@Getter
@Setter
public class Secret {
    public static final int VERSION = 1;

    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true, populate = true)
    private String id;

    @PostgresField(type = PostgresFieldType.TEXT)
    private String clearText;
    @PostgresField(type = PostgresFieldType.TEXT, securityMethod = ObfuscateMethod.OBFUSCATED_WRITE)
    private String secret1;
    @PostgresField(type = PostgresFieldType.TEXT, securityMethod = ObfuscateMethod.OBFUSCATED_WRITE_READ)
    private String secret2;
    @PostgresField(type = PostgresFieldType.TEXT, securityMethod = ObfuscateMethod.SALTED_WRITE)
    private String salted;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
