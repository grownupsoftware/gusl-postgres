package gusl.postgres;

import gusl.core.tostring.ToString;
import lombok.*;

/**
 * @author dhudson
 * @since 28/09/2022
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Bar {
    private Long id;
    private String name;

    public String toString() {
        return ToString.toString(this);
    }
}
