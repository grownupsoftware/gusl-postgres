package gusl.postgres;

import gusl.postgres.application.MigrateConfig;
import gusl.postgres.application.PostgresEvolve;
import gusl.postgres.config.PostgresConfig;
import gusl.postgres.exceptions.PostgresEvolutionException;
import lombok.CustomLog;
import org.junit.Test;

/**
 * @author dhudson
 * @since 13/04/2022
 */
@CustomLog
public class EvolveTest {

    @Test
    public void evolveTester() {

        PostgresConfig config = new PostgresConfig();
        config.setDatabase("alvar");
        config.setPassword("password");
        config.setUsername("postgres");
        config.setConnectionPoints("localhost");

        MigrateConfig migrateConfig = new MigrateConfig();
        migrateConfig.setRunInstanceScripts(false);
        migrateConfig.setClean(false);
        migrateConfig.setMode("DEV");
        migrateConfig.setInstance("instance");
        PostgresEvolve evolve = new PostgresEvolve(config, migrateConfig);


        try {
            evolve.evolve();
        } catch (PostgresEvolutionException ex) {
            logger.warn("Nope ...  ", ex);
        }
    }
}
