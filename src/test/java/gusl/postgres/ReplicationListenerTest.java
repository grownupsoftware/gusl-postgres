package gusl.postgres;

import gusl.core.exceptions.GUSLErrorException;
import gusl.postgres.cdc.CDCDigester;
import gusl.postgres.cdc.ReplicationConnection;
import gusl.postgres.config.PostgresConfig;
import lombok.CustomLog;
import org.junit.Test;

import java.sql.SQLException;

/**
 * @author dhudson
 * @since 28/07/2021
 */
@CustomLog
public class ReplicationListenerTest {

    // https://github.com/hellobike/tunnel/blob/master/tunnel-server/src/main/java/com/hellobike/base/tunnel/TunnelServer.java#L135
    // https://www.tabnine.com/web/assistant/code/rs/5c66ee741095a50001ea368f#L93
    // https://github.com/hellobike/tunnel/blob/master/tunnel-server/src/main/java/com/hellobike/base/tunnel/TunnelServer.java#L135
    // https://github.com/pgjdbc/pgjdbc/commit/f48c6bb7e726479bbf4be4ef95e4c138db5cac96
    // https://debezium.io/documentation/reference/1.6/connectors/postgresql.html#setting-up-postgresql


    @Test
    public void replicationListener() throws GUSLErrorException {

        try {
            PostgresConfig config = new PostgresConfig();
            config.setDatabase("supernova");
            config.setPassword("replication");
            config.setUsername("replication");
            config.setConnectionPoints("localhost");
            config.setReplication("database");

            ReplicationConnection replicationConnection = new ReplicationConnection();
            replicationConnection.connect(config);

            replicationConnection.initPublication();
            logger.info("Slots {}", replicationConnection.getReplicationSlots());

            CDCDigester digester = new CDCDigester(replicationConnection.createReplicationStream("table_slot1"), replicationConnection.getTableMap());

            digester.start();
            digester.join();


        } catch (SQLException | InterruptedException ex) {
            logger.warn("Unable to stream", ex);
        }
    }


}

