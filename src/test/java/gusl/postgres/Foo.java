package gusl.postgres;

import gusl.core.tostring.ToString;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * @author dhudson
 * @since 13/04/2022
 */
@Getter
@Setter
@NoArgsConstructor
public class Foo {

    public static final int VERSION = 6;
    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true)
    private String id;
    @PostgresField(type = PostgresFieldType.TEXT, defaultValue = "Hello")
    private String staticData;
    @PostgresField(type = PostgresFieldType.BOOLEAN, defaultValue = "FALSE")
    private Boolean populatedBoolean;
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateCreated = true, populate = true)
    private ZonedDateTime created;
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateUpdated = true, populate = true)
    private ZonedDateTime updated;
    @PostgresField(type = PostgresFieldType.TIMESTAMP)
    private LocalDateTime localCreated;
    @PostgresField(type = PostgresFieldType.TIMESTAMP)
    private LocalDateTime localUpdated;
    @PostgresField(type = PostgresFieldType.NUMERIC)
    private Double price;
    @PostgresField(type = PostgresFieldType.NUMERIC)
    private BigDecimal ask;
    @PostgresField(type = PostgresFieldType.LIST)
    private List<String> accounts;
    @PostgresField(type = PostgresFieldType.LIST)
    private List<Bar> bars;

    public String toString() {
        return ToString.toString(this);
    }
}
