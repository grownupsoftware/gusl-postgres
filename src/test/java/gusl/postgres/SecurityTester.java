package gusl.postgres;

import gusl.core.exceptions.GUSLErrorException;
import gusl.postgres.config.PostgresConfig;
import gusl.postgres.connection.PostgresConnectionImpl;
import lombok.CustomLog;
import org.junit.Test;

/**
 * @author dhudson
 * @since 17/05/2022
 */
@CustomLog
public class SecurityTester {

    @Test
    public void securityTest() throws GUSLErrorException {

        PostgresConfig config = new PostgresConfig();
        config.setDatabase("alvar");
        config.setPassword("password");
        config.setUsername("postgres");
        config.setConnectionPoints("localhost");

        PostgresConnectionImpl connection = new PostgresConnectionImpl();
        connection.connect(config);

        SecretTable table = new SecretTable();
        table.setPostgresConnection(connection);
        table.config();

        Secret secret = new Secret();
        secret.setId("2");
        secret.setClearText("Hello World");
        secret.setSecret1("Hello World");
        secret.setSecret2("Hello World");
        secret.setSalted("Hello World");

        table.insert(secret);

        secret = table.findRecord("1");
        logger.info("{}", secret);

    }
}
